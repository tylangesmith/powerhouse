# powerhouse

Developed by: 
- Ty Lange-Smith
- https://au.linkedin.com/in/tylangesmith

A hosted version of the project can be found at (Please note that this is my development hosting and could break with earlier versions of data within the system as this project is rapidly changing): 
- https://tylange1.github.io/

This project is now deployed for both Android and iOS!

Andriod: https://play.google.com/store/apps/details?id=com.ionicframework.powerhouseapp458179

iOS: https://appsto.re/au/fvQ1hb.i

## What is it?

Powerhouse is a gym progress tracking program designed to be used of tablets and mobile phones. 
Users are able to create custom gym programs based off of default or custom program types and track their progress. 
The main features of the program are:
- Create custom programs based off of a program type
- Complete sets within a progam
- Create custom program types (Now including supersets)
- Edit programs and program types
- Remove programs and program types
- Undo removal of programs and program types
- Quick complete sets in most recently active program
- Convert between kgs and lbs
- Filter by programs and programTypes by a text filter
- Sort programs and programTypes by a property
- Persist sorting preferences between program invocations
- Reset all program data
- Calculate ORM based off of a weight and number of repetitions
- Barbell plate calculator

## Technologies

This project is developed using the Angular framework using yeoman to ease and accelerate the development process.

## Running

A hosted version of the project can be found at: 
- https://tylange1.github.io/

For the best experience ensure that the program is run on a smaller screen size i.e a mobile / tablet device. 
This can be achieved by either going to the above hosting link on a mobile device or activating developer device emulation within the browser.

To run the program execute the following command `grunt serve`.
