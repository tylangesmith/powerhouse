'use strict';

/**
 * @ngdoc overview
 * @name powerHouseApp
 * @description
 * # powerHouseApp
 *
 * Main module of the application.
 */
angular
  .module('powerHouseApp', [
    'ngMaterial',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'LocalStorageModule',
    'material.components.expansionPanels'
  ])
  .config(function ($routeProvider, localStorageServiceProvider, $mdThemingProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/dashboard.html',
        controller: 'dashboardCtrl',
        controllerAs: 'dashboard'
      })
      .when('/program-list', {
        templateUrl: 'views/programList.html',
        controller: 'programListCtrl',
        controllerAs: 'programList'
      })
      .when('/add-program', {
        templateUrl: 'views/addProgram.html',
        controller: 'addProgramCtrl',
        controllerAs: 'addProgram'
      })
      .when('/add-program-type', {
        templateUrl: 'views/addProgramType.html',
        controller: 'addProgramTypeCtrl',
        controllerAs: 'addProgramType'
      })
      .when('/program-type-list', {
        templateUrl: 'views/programTypeList.html',
        controller: 'ProgramTypeListCtrl',
        controllerAs: 'programTypeList'
      })
      .when('/program-type-information/:id', {
        templateUrl: 'views/programTypeInformation.html',
        controller: 'ProgramTypeInformationCtrl',
        controllerAs: 'programTypeInformation'
      })
      .when('/program-information/:id', {
        templateUrl: 'views/programInformation.html',
        controller: 'ProgramInformationCtrl',
        controllerAs: 'programInformation'
      })
      .when('/edit-program/:id', {
        templateUrl: 'views/editProgram.html',
        controller: 'EditProgramCtrl',
        controllerAs: 'editProgram'
      })
      .when('/edit-program-type/:id', {
        templateUrl: 'views/editProgramType.html',
        controller: 'EditProgramTypeCtrl',
        controllerAs: 'editProgramType'
      })
      .when('/settings', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl',
        controllerAs: 'settings'
      })
      .when('/info', {
        templateUrl: 'views/info.html',
        controller: 'InfoCtrl',
        controllerAs: 'info'
      })
      .when('/upgrade', {
        templateUrl: 'views/upgrade.html',
        controller: 'UpgradeCtrl',
        controllerAs: 'upgrade'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'contact'
      })
      .when('/help', {
        templateUrl: 'views/help.html',
        controller: 'HelpCtrl',
        controllerAs: 'help'
      })
      .when('/one-rep-max-calculator', {
        templateUrl: 'views/oneRepMaxCalculator.html',
        controller: 'OneRepMaxCalculatorCtrl',
        controllerAs: 'oneRepMaxCalculator'
      })
      .when('/barbell-plate-calculator', {
        templateUrl: 'views/barbellPlateCalculator.html',
        controller: 'BarbellPlateCalculatorCtrl',
        controllerAs: 'barbellPlateCalculator'
      })
      .otherwise({
        redirectTo: '/'
      });

      localStorageServiceProvider.setPrefix('PowerHouse');

      $mdThemingProvider.theme('default')
        .primaryPalette('blue-grey')
        .accentPalette('deep-orange');
  })
  .run(function(tutorialService){
    tutorialService.showInitialTutorial();
  });