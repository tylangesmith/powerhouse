'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.adWeightService
 * @description
 * # adWeightService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('adWeightService', function () {
    var contract = {};

    contract.weights = {
      'addProgram': 20,
      'addProgramType': 20,
      'editProgram': 20,
      'editProgramType': 20,
      'completeProgram': 20,
      'completeDay': 20
    };

    return contract;
  });
