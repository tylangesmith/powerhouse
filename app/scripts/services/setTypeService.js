'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.setTypeService
 * @description
 * # setTypeService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('setTypeService', function () {
    var contract = {};

    contract.types = [
      {
        id: 0,
        name: 'Normal'
      },
      {
        id: 1,
        name: 'Superset'
      },
    ];

    contract.getDefault = function(){
      return contract.types[0];
    };

    return contract;
  });
