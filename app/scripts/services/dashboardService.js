'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.dashboardService
 * @description
 * # dashboardService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('dashboardService', ['programService', 'recentlyActiveService', function (programService, recentlyActiveService) {
    var contract = {
      completedHeaderText: 'Programs',
      completedHighlightText: programService.getCompletedPrograms().length,
      completedHighlightColor: 'green-500',
      completeSubheadText: 'complete',
      
      activeHeaderText: 'Programs',
      activeHighlightText: programService.getActivePrograms().length,
      activeHighlightColor: 'deep-orange-500',
      activeSubheadText: 'active',
      
      quickCompleteProgram: recentlyActiveService.getRecentlyActive()
    };

    contract.getCompletedPrograms = function(){
      var completedPrograms = programService.getCompletedPrograms().length;
      if(changed(contract.completedHighlightText, completedPrograms)){
        contract.completedHighlightText = completedPrograms;
      }
      return completedPrograms;
    };

    contract.getActivePrograms = function(){
      var activePrograms = programService.getActivePrograms();
      if(changed(contract.activeHighlightText, activePrograms.length)){
        contract.activeHighlightText = activePrograms.length;
      }
      return activePrograms;
    };

    contract.getQuickCompleteProgram = function(){
      var quickCompleteProgram = recentlyActiveService.getRecentlyActive();
      if(changed(contract.quickCompleteProgram, quickCompleteProgram)){
        contract.quickCompleteProgram = quickCompleteProgram;
      }
      return quickCompleteProgram;
    };

    var changed = function(value1, value2){
      return value1 !== value2;
    };

    return contract;
  }]);
