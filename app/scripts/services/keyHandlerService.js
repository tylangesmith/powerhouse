'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.keyHandlerService
 * @description
 * # keyHandlerService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('keyHandlerService', function () {
    var contract = {};

    contract.keys = {
      tutorialComplete: 'TUTORIAL_COMPLETE',
      programType: 'PROGRAM_TYPE',
      program: 'PROGRAM',
      recentlyActive: 'RECENTLY_ACTIVE',
      unit: 'WEIGHT_UNIT',
      programListOrderKey: 'PROGRAM_LIST_ORDER_KEY',
      programListReversedKey: 'PROGRAM_LIST_REVERSED_KEY',
      programTypeListOrderKey: 'PROGRAM_TYPE_LIST_ORDER_KEY',
      programTypeListReversedKey: 'PROGRAM_TYPE_LIST_REVERSED_KEY',
      barbellPlateCalculatorBarWeight: 'BARBELL_PLATE_CALCULATOR_BAR_WEIGHT_KEY',
      barbellPlateCalculatorCurrentWeight: 'BARBELL_PLATE_CALCULATOR_CURRENT_WEIGHT_KEY',
    };

    return contract;
  });
