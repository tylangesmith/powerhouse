'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.oneRepMaxCalculatorService
 * @description
 * # oneRepMaxCalculatorService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('oneRepMaxCalculatorService', ['utilService', 'unitService', function (utilService, unitService) {
    var contract = {
      helpOneRepMaxCalculatorUrl: 'views/helpOneRepMaxCalculator.html'
    };

    var init = function(){
      contract.reset();
    };

    contract.reset = function(){
      contract.oRMPercentages = [];
      for(var i = 100; i > 0; i = i - 5){
        contract.oRMPercentages.push({
          name: i + '% ORM',
          percentage: i / 100,
          value: 0
        });
      }
    };

    contract.getUnit = function(){
      return unitService.getCurrentUnit();
    };

    contract.getORMPercentages = function(){
      return contract.oRMPercentages;
    };

    contract.calculateORMPercentages = function(weightLifted, numberOfReps){
      var oRM = 0;
      if(valuesInvalid(weightLifted, numberOfReps) === false){
        oRM = calculateORM(weightLifted, numberOfReps);
      }
      
      contract.oRMPercentages = calculatePercentagesOfORM(oRM, contract.oRMPercentages);
    };

    var calculatePercentagesOfORM = function(oRM, percentages){
      return percentages.map(function(percentage){
        percentage.value = utilService.round((oRM * percentage.percentage), 0, 1);
        return percentage;
      });
    };

    var calculateORM = function(weightLifted, numberOfReps){
      return utilService.round((weightLifted / (1.0278 - (0.0278 * numberOfReps) )), 0, 1);
    };

    var valuesInvalid = function(weightLifted, numberOfReps){
      return (utilService.isUndefined(weightLifted) || !utilService.isNumber(weightLifted) || 
      weightLifted === '' || weightLifted <= 0 || utilService.isUndefined(numberOfReps) || 
      !utilService.isNumber(numberOfReps) || numberOfReps === '' || numberOfReps <= 0 || numberOfReps > 12);
    };

    init();

    return contract;
  }]);
