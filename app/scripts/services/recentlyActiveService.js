'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.recentlyActiveService
 * @description
 * # recentlyActiveService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('recentlyActiveService', ['utilService', 'storageService', 'keyHandlerService', function (utilService, storageService, keyHandlerService) {
    var contract = {
      recentlyActive: {},
      removedProgram: {}
    };

    var init = function(){
      contract.recentlyActive = getRecentlyActiveFromStorage();
    };

    contract.reset = function(){
      contract.updateRecentlyActive({});
    };

    contract.getRecentlyActive = function(){
      return contract.recentlyActive;
    };

    contract.updateRecentlyActive = function(recentlyActive){
      var mostRecent = {};

      // Not Complete
      if(defined(recentlyActive) && recentlyActive.complete === false){
        mostRecent = recentlyActive;
      }

      contract.recentlyActive = mostRecent;
      storeRecentlyActive();
    };

    contract.undoRemove = function(){
      contract.recentlyActive = contract.removedProgram;
      contract.removedProgram = {};
    };

    contract.removeProgram = function(program){
      if(contract.recentlyActive.id === program.id){
        contract.removedProgram = program;
        contract.updateRecentlyActive({});
      }
    };

    contract.nextActive = function(programs){
      if(programs.length === 0){
        return {};
      }
      // Can later make this more intelligent to tell which one of the
      // active programs was the next most active
      return programs[0];
    };

    contract.currentlyActive = function(){
      return defined(contract.recentlyActive);
    };

    var defined = function(recentlyActive){
      return (utilService.isDefined(recentlyActive) && !angular.equals(recentlyActive, {}));
    };

    var getRecentlyActiveFromStorage = function(){
      return storageService.getValueOrDefault(keyHandlerService.keys.recentlyActive, {});
    };

    var storeRecentlyActive = function(){
      storageService.storeValue(keyHandlerService.keys.recentlyActive, contract.recentlyActive);
    };

    init();

    return contract;
  }]);
