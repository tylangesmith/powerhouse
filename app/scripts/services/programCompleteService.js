'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.programCompleteService
 * @description
 * # programCompleteService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('programCompleteService', ['toastService', 'adTriggerService', 'adWeightService', function (toastService, adTriggerService, adWeightService) {
    var contract = {};

    contract.programComplete = function(completeProgram){
      toastService.showProgramCompleteToast('Well done, ' + completeProgram.name + ' complete', programCompleteAdFunction);
    };

    contract.dayComplete = function(day){
      toastService.showProgramCompleteToast('Well done, ' + day.name + ' complete', dayCompleteAdFunction);
    };

    var programCompleteAdFunction = function(){
      adTriggerService.incrementCount(adWeightService.weights.completeProgram);
    };

    var dayCompleteAdFunction = function(){
      adTriggerService.incrementCount(adWeightService.weights.completeDay);
    };

    return contract;
  }]);
