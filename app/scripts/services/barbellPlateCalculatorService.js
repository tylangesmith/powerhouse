'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.barbellPlateCalculatorService
 * @description
 * # barbellPlateCalculatorService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('barbellPlateCalculatorService', ['unitService', 'utilService', 'storageService', 'keyHandlerService', function (unitService, utilService, storageService, keyHandlerService) {
    var contract = {
      helpUrl: 'views/helpBarbellPlateCalculator.html',
      maxAvailable: 20,
      plates: [],
      currentWeights: {
        units: unitService.getCurrentUnit(),
        weights: []
      },
      barbellWeight: 0, 
      weights: {
        kilograms: [
          {
            id: 0,
            value: 50,
            available: 10,
            enabled: true,
            color: 'deep-purple-500',
          },
          {
            id: 1,
            value: 25,
            available: 10,
            enabled: true,
            color: 'red-500'
          },
          {
            id: 2,
            value: 20,
            available: 10,
            enabled: true,
            color: 'blue-500'
          },
          {
            id: 3,
            value: 15,
            available: 10,
            enabled: true,
            color: 'yellow-500'
          },
          {
            id: 4,
            value: 10,
            available: 10,
            enabled: true,
            color: 'green-500'
          },
          {
            id: 5,
            value: 5,
            available: 10,
            enabled: true,
            color: 'grey-900'
          },
          {
            id: 6,
            value: 2.5,
            available: 10,
            enabled: true,
            color: 'orange-500'
          },
          {
            id: 7,
            value: 1.25,
            available: 10,
            enabled: true,
            color: 'grey-500'
          }
        ],
        pounds: [
          {
            id: 0,
            value: 110,
            available: 10,
            enabled: true,
            color: 'deep-purple-500'
          },
          {
            id: 1,
            value: 55,
            available: 10,
            enabled: true,
            color: 'red-500'
          },
          {
            id: 2,
            value: 45,
            available: 10,
            enabled: true,
            color: 'blue-500'
          },
          {
            id: 3,
            value: 35,
            available: 10,
            enabled: true,
            color: 'yellow-500'
          },
          {
            id: 4,
            value: 25,
            available: 10,
            enabled: true,
            color: 'green-500'
          },
          {
            id: 5,
            value: 10,
            available: 10,
            enabled: true,
            color: 'grey-900'
          },
          {
            id: 6,
            value: 5,
            available: 10,
            enabled: true,
            color: 'orange-500'
          },
          {
            id: 7,
            value: 2.5,
            available: 10,
            enabled: true,
            color: 'grey-500'
          }
        ]
      }
    };

    var init = function(){
      contract.currentWeights = getCurrentWeightsFromStorage();
      contract.barbellWeight = getBarbellWeightFromStorage();
      contract.plates = angular.copy(contract.currentWeights.weights);
    };

    contract.getCurrentUnit = function(){
      return unitService.getCurrentUnit();
    };

    contract.getCurrentWeights = function(){
      // Check if the unit used differs from the global unit
      if(unitChanged(contract.getCurrentUnit(), contract.currentWeights.units)){
        contract.updateBarbellWeight(0);
        contract.updateCurrentWeights({ units: contract.getCurrentUnit(), weights: contract.weights[contract.getCurrentUnit().name] });
        contract.plates = angular.copy(contract.currentWeights.weights);
      }
      return contract.currentWeights;
    };

    contract.getBarbellWeight = function(){
      return contract.barbellWeight;
    };

    contract.calculatePlates = function(targetWeight){
      if(isInvalid(targetWeight, contract.barbellWeight, contract.currentWeights.weights) === true){
        contract.plates = angular.copy(contract.currentWeights.weights);
        return [];
      }

      var plates = [];
      var currentWeight = ((targetWeight - contract.barbellWeight) / 2);
        
      // For every plate denomination
      for(var i = 0; i < contract.currentWeights.weights.length; i++){
        var plate = angular.copy(contract.currentWeights.weights[i]);

        // Check that the plate is used and that it is less than the currentWeight
        if(plate.enabled === true && plate.value <= currentWeight){
          
          // Find how many times we can divide into the value
          var multiples = Math.floor(currentWeight / plate.value);
          
          // Check if there is enough plates available for both sides
          if(multiples > (plate.available / 2)){
            var available = plate.available;
            if(available % 2 !== 0){
              available = available - 1;
            }
            multiples = available / 2;            
          }

          // Update the currentWeight
          currentWeight = currentWeight - (multiples * plate.value);
          plate.number = multiples;
        }
        plates.push(plate);
      }

      if(currentWeight !== 0){
        plates = contract.currentWeights.weights;
      }
      
      contract.plates = plates;
    };

    contract.updateBarbellWeight = function(barbellWeight){
      contract.barbellWeight = barbellWeight;
      storeBarbellWeight(barbellWeight);
    };

    contract.updateCurrentWeights = function(currentWeights){
      contract.currentWeights = currentWeights;
      storeCurrentWeights(currentWeights);
    };

    contract.getRange = function(){
      var rArray = [];
      for(var i = 0; i < contract.maxAvailable; i++){
        rArray.push(i);
      }
      return rArray;
    };

    contract.getPlates = function(){
      return contract.plates;
    };

    var isInvalid = function(targetWeight, barbellWeight, currentWeights){
      return ((utilService.isUndefined(targetWeight) || targetWeight === '' || targetWeight < 0 || targetWeight % contract.getCurrentUnit().interval !== 0) || 
      (utilService.isUndefined(barbellWeight) || barbellWeight === '' || barbellWeight < 0 || barbellWeight % contract.getCurrentUnit().interval !== 0) || 
      (utilService.isUndefined(currentWeights) || currentWeights.length === 0));
    };

    var unitChanged = function(globalUnit, compareUnit){
      return globalUnit.name !== compareUnit.name;
    };

    var getBarbellWeightFromStorage = function(){
      return storageService.getValueOrDefault(keyHandlerService.keys.barbellPlateCalculatorBarWeight, 0);
    };

    var storeBarbellWeight = function(barbellWeight){
      storageService.storeValue(keyHandlerService.keys.barbellPlateCalculatorBarWeight, barbellWeight);
    };

    var getCurrentWeightsFromStorage = function(){
      var defaultValue = { units: contract.getCurrentUnit(), weights: contract.weights[contract.getCurrentUnit().name] };
      var currentWeight = storageService.getValueOrDefault(keyHandlerService.keys.barbellPlateCalculatorCurrentWeight , defaultValue);

      // Check if the units have changed
      if(unitChanged(currentWeight.units, contract.getCurrentUnit())){
        currentWeight = defaultValue;
      }
      return currentWeight;
    };

    var storeCurrentWeights = function(currentWeights){
      storageService.storeValue(keyHandlerService.keys.barbellPlateCalculatorCurrentWeight, currentWeights);
    };

    init();

    return contract;
  }]);
