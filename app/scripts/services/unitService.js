'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.unitService
 * @description
 * # unitService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('unitService', ['utilService', 'storageService', 'keyHandlerService', function (utilService, storageService, keyHandlerService) {
    var contract = {
      currentUnit: 'kilograms',
      unitMap: {
        'kilograms': {
          name: 'kilograms',
          textName: 'kgs',
          interval: 2.5,
          conversion: 2.20462,
          conversionInterval: 5
        },
        'pounds': {
          name: 'pounds',
          textName: 'lbs',
          interval: 5,
          conversion: 0.453592,
          conversionInterval: 2.5
        }
      }
    };

    var init = function(){
      contract.currentUnit = getUnitFromStorage();
    };

    contract.reset = function(){
      contract.changeUnit('kilograms');
    };

    contract.changeUnit = function(key){
      contract.currentUnit = key;
      storeUnit();
    };

    contract.getUnits = function(){
      return Object.keys(contract.unitMap).map(function(unit){
        return contract.unitMap[unit];
      });
    };

    contract.getCurrentUnit = function(){
      return contract.unitMap[contract.currentUnit];
    };

    contract.convert = function(key, weight){
      var unit = contract.unitMap[key];
      return utilService.round(weight * unit.conversion, 1, unit.conversionInterval);
    };

    var getUnitFromStorage = function(){
      return storageService.getValueOrDefault(keyHandlerService.keys.unit, 'kilograms');
    };

    var storeUnit = function(){
      storageService.storeValue(keyHandlerService.keys.unit, contract.currentUnit);
    };

    init();

    return contract;
  }]);
