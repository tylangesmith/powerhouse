'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.programService
 * @description
 * # programService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('programService', ['utilService', 'storageService', 'keyHandlerService', 'toastService', 'recentlyActiveService', 'unitService', 'programConversionService', 'programCompleteService',
    function (utilService, storageService, keyHandlerService, toastService, recentlyActiveService, unitService, programConversionService, programCompleteService) {
    var contract = {
      programs: [],
    };

    var init = function(){
      contract.programs = getProgramsFromStorage();
      contract.convertPrograms();
      if(recentlyActiveService.currentlyActive() === false){
        recentlyActiveService.updateRecentlyActive(recentlyActiveService.nextActive(contract.getActivePrograms()));
      }
    };

    contract.reset = function(){
      contract.programs = [];
      storePrograms();
    };

    contract.convertPrograms = function(){
      contract.programs = programConversionService.convertPrograms(contract.programs);
      if(recentlyActiveService.currentlyActive() === false){
        recentlyActiveService.updateRecentlyActive(recentlyActiveService.nextActive(contract.getActivePrograms()));
      }
      else {
        // Find the corresponding program and set it as the new recently getActivePrograms
        recentlyActiveService.updateRecentlyActive(contract.programs[findProgramIndex(recentlyActiveService.getRecentlyActive())]); 
      }
      storePrograms();
    };

    contract.addProgram = function(programName, programType, increment, exercises){
      var program = generateProgram(utilService.getUniqueId(contract.programs), programName, programType, increment, exercises);
      contract.programs.push(program);
      contract.convertPrograms();
      recentlyActiveService.updateRecentlyActive(program);
      storePrograms();
    };

    contract.editProgram = function(oldProgram, id, programName, programType, increment, exercises){
      removeProgramWithoutToast(oldProgram);
      var program = mapProgramCompletion(oldProgram, generateProgram(id, programName, programType, increment, exercises));
      contract.programs.push(program);
      contract.updateProgramComplete(program);
      recentlyActiveService.updateRecentlyActive(program);
      storePrograms();
    };

    contract.removeProgram = function(program){
      var tempPrograms = angular.copy(contract.programs);

      toastService.showUndoToast('Program removed', function(){        
        contract.programs = tempPrograms;
        recentlyActiveService.undoRemove();
        storePrograms();
      });

      recentlyActiveService.removeProgram(program);
      removeProgram(program);
      recentlyActiveService.updateRecentlyActive(recentlyActiveService.nextActive(contract.getActivePrograms()));
    };

    contract.getPrograms = function(){
      return contract.programs;
    };

    contract.getProgram = function(id){
      var index = findProgramIndex({id: id});
      return contract.programs[index];
    };

    contract.nameTaken = function(programName){
      for(var i = 0; i < contract.programs.length; i++){
        if(utilService.isDefined(contract.programs[i].name) && contract.programs[i].name === programName){
          return true;
        }
      }
      return false;
    };

    contract.updateProgram = function(program){
      var index = findProgramIndex(program);
      contract.programs[index] = program;

      if(program.complete === false){
        recentlyActiveService.updateRecentlyActive(program);
      }
      else {
        recentlyActiveService.updateRecentlyActive(recentlyActiveService.nextActive(contract.getActivePrograms()));
      }

      storePrograms();
      return program;
    };

    contract.updateProgramComplete = function(program, day){
      var updatedProgram = program;

      var completedSets = 0;

      updatedProgram.weeks.forEach(function(week){
        week.days.forEach(function(day){
          day.sets.forEach(function(set){
            if(set.complete === true){
              completedSets += set.numberOfSets;
            }
          });
        });
      });

      updatedProgram.percentComplete = calculatePercentage(program.programType.totalNumberOfSets, completedSets);

      var previouslyComplete = updatedProgram.complete;
      updatedProgram.complete = completeProgram(program.programType.totalNumberOfSets, completedSets);
      var newlyComplete = updatedProgram.complete;

      // Program is newly completed
      if(previouslyComplete !== newlyComplete && updatedProgram.complete === true){
        programCompleteService.programComplete(updatedProgram);
      }
      else if(utilService.isDefined(day) && allSetsCompleteInDay(day)){
        // Check if we have completed all sets for today, if we have show an ad
        programCompleteService.dayComplete(day);
      }

      return contract.updateProgram(updatedProgram);
    };
    
    contract.getCompletedPrograms = function(){
      return contract.programs.filter(function(program){
        return program.complete === true;
      });
    };

    contract.getActivePrograms = function(){
      return contract.programs.filter(function(program){
        return (program.complete === false || utilService.isUndefined(program.complete));
      });
    };
    
    var allSetsCompleteInDay = function(day){
      for(var i = 0; i < day.sets.length; i++){
        if(day.sets[i].complete === false){
          return false;
        }
      }
      return true;
    };

    var completeProgram = function(totalSets, completedSets){
      return (totalSets === completedSets);
    };

    var removeProgram = function(program){
      contract.programs = utilService.removeFromArray(contract.programs, program);
      storePrograms();
    };

    var removeProgramWithoutToast = function(program){
      removeProgram(program);
    };

    var mapProgramCompletion = function(oldProgram, newProgram){
      var program = newProgram;

      if(program.programType.id === oldProgram.programType.id){
        oldProgram.weeks.forEach(function(week, wIndex){
          week.days.forEach(function(day, dIndex){
            day.sets.forEach(function(set, sIndex){
              if(set.complete === true){
                program.weeks[wIndex].days[dIndex].sets[sIndex].complete = true;
              }
            });
          });
        });
      }

      return program;
    };

    var calculatePercentage = function(total, complete){
      return ((complete / total) * 100);
    };

    var findProgramIndex = function(program){
      return contract.programs.findIndex(function(p){
        return utilService.getNumber(program.id) === utilService.getNumber(p.id);
      });
    };

    var generateProgram = function(id, programName, programType, increment, exercises){
      return {
        id: id,
        unit: unitService.getCurrentUnit().name,
        name: programName,
        weeks: generateWeeks(programType, increment, exercises),
        percentComplete: 0,
        programType: programType,
        increment: utilService.getNumber(increment).toFixed(1),
        exercises: exercises,
        default: false,
        complete: false
      };
    };

    var generateWeeks = function(programType, increment, exercises){
      return programType.weeks.map(function(week){
        return {
          id: week.id,
          name: week.name,
          days: generateDays(week, increment, exercises)
        };
      });
    };

    var generateDays = function(week, increment, exercises){
      return week.days.map(function(day){
        return {
          id: day.id,
          name: day.name,
          sets: generateSets(day, increment, exercises)
        };
      });
    };

    var generateSets = function(day, increment, exercises){
      return day.sets.map(function(set){
        return {
          id: set.id,
          setType: set.setType,
          numberOfSets: set.numberOfSets,
          exercises: generateSetExercises(set, increment, exercises),
          complete: false
        };
      });
    };

    var generateSetExercises = function(set, increment, exercises){
        return set.exercises.map(function(setExercise){
          if(setExercise.exercise.exerciseType.id === 0){
            setExercise.weight = generateWeight((setExercise.oneRepMaxPercentage * 0.01), setExercise.incrementMultiplier, increment, setExercise.exercise.id, exercises);
          }
          return setExercise;
        });
    };

    var generateWeight = function(oneRepMaxPercent, incrementMultiplier, increment, exerciseId, exercises){
      // (ORMP * exerciseORM) + increment
      return utilService.round((oneRepMaxPercent * findOneRepMax(exerciseId, exercises)) + (utilService.getNumber(increment) * incrementMultiplier), 1, 2.5);
    };

    var findOneRepMax = function(exerciseId, exercises){
      for(var i = 0; i < exercises.length; i++){
        if(exercises[i].id === exerciseId){
          return utilService.getNumber(exercises[i].oneRepMax);
        }
      }
      return -1;
    };

    var getProgramsFromStorage = function(){
      return storageService.getValueOrDefault(keyHandlerService.keys.program, []);
    };

    var storePrograms = function(){
      storageService.storeValue(keyHandlerService.keys.program, contract.programs);
    };

    init();
    return contract;
  }]);
