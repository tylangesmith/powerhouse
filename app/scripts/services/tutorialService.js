'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.tutorialService
 * @description
 * # tutorialService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('tutorialService', ['storageService', 'keyHandlerService', '$mdDialog', function (storageService, keyHandlerService, $mdDialog) {
    var contract = {};

    contract.showInitialTutorial = function(){
      if(tutorialShown() === false){
        displayTutorial();
      }
    };

    contract.showTutorial = function(){
      displayTutorial();
    };

    var displayTutorial = function(){
      $mdDialog.show({
        controller: 'tutorialController',
        templateUrl: 'views/tutorialModal.html',
        ariaLabel: 'Tutorial Dialog',
        clickOutsideToClose: false
      });
    };

    var tutorialShown = function(){
      return storageService.getValueOrDefault(keyHandlerService.keys.tutorialComplete, false);
    };

    var tutorialComplete = function(){
      storageService.storeValue(keyHandlerService.keys.tutorialComplete, true);
    };

    return contract;
  }]);
