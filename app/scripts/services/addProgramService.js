'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramService
 * @description
 * # addProgramService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramService', ['utilService', 'programService', 'exerciseTypeService', 'unitService', 'adTriggerService', 'adWeightService', function (utilService, programService, exerciseTypeService, unitService, adTriggerService, adWeightService) {
    var contract = {};

    contract.addProgram = function(programName, programType, increment, exercises){
      adTriggerService.incrementCount(adWeightService.weights.addProgram);
      programService.addProgram(programName, programType, increment, formatExercises(programType.exercises, exercises));
    };

    contract.editProgram = function(program, id, programName, programType, increment, exercises){
      adTriggerService.incrementCount(adWeightService.weights.editProgram);
      programService.editProgram(program, id, programName, programType, increment, formatExercises(programType.exercises, exercises));
    };

    contract.invalid = function(programName, programType, increment, exercises){
      return (programNameInvalid(programName) || programNameDuplicateInvalid(programName) || 
      programTypeInvalid(programType) || programIncrementInvalid(increment) || programExercisesInvalid(exercises));
    };

    contract.invalidAllowDuplicateNames = function(programName, programType, increment, exercises){
      return (programNameInvalid(programName) || programTypeInvalid(programType) || 
      programIncrementInvalid(increment) || programExercisesInvalid(exercises));
    };

    contract.getExercises = function(programType){
      // Return all the required one rep max exercises
      var rArray = [];

      programType.exercises.forEach(function(exercise){
        if(exercise.exerciseType.id === 0){
          rArray.push(exercise);
        }
      });

      return rArray;
    };

    contract.getExercisesWithORM = function(programType, program){
      var programTypeExercises = contract.getExercises(programType);
      var programExercises = contract.getExercises(program);

      for(var i = 0; i < programTypeExercises.length; i++){
        for(var y = 0; y < programExercises.length; y++){
          if(exerciseTypeService.exercisesSame(programTypeExercises[i], programExercises[y])){
            programTypeExercises[i].oneRepMax = programExercises[y].oneRepMax;
            break;
          }
        }        
      }

      return programTypeExercises;
    };

    var formatExercises = function(originalExercises, newExercises){
      var fExercises = originalExercises;

      var findIndex = function(array, id){
        return array.findIndex(function(value){
          return value.id === id;
        });
      };

      for(var i = 0; i < newExercises.length; i++){
        fExercises[findIndex(fExercises, newExercises[i].id)] = newExercises[i];
      }

      return fExercises; 
    };

    var programNameInvalid = function(programName){
      var invalid = false;

      if(programName === '' || utilService.isUndefined(programName)){
        invalid = true;
      }

      return invalid;
    };

    var programNameDuplicateInvalid = function(programName){
      var invalid = false;

      if(programService.nameTaken(programName)){
        invalid = true;
      }

      return invalid;
    };

    var programTypeInvalid = function(programType){
      var invalid = false;

      if(programType === '' || utilService.isUndefined(programType)){
        invalid = true;
      }

      return invalid;
    };

    var programIncrementInvalid = function(increment){
      var invalid = false;

      if(utilService.isNumber(increment) === false){
        invalid = true;
      }
      else if(increment < 0){
        invalid = true;
      }
      else if(increment !== 0 && increment % unitService.getCurrentUnit().interval !== 0){
        invalid = true;
      }

      return invalid;
    };

    var programExercisesInvalid = function(exercises){
      var invalid = false;

      for(var i = 0; i < exercises.length; i++){
        var exercise = exercises[i];
        if(utilService.isUndefined(exercise.oneRepMax) || exercise.oneRepMax < 1){
          invalid = true;
          break;
        }
      }

      return invalid;
    };

    return contract;
  }]);
