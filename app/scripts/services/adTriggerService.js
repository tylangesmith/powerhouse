'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.adTriggerService
 * @description
 * # adTriggerService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('adTriggerService', function () {
    var contract = {
      threshold: 20,
      count: 0
    };

    contract.incrementCount = function(weight){
      contract.count += weight;
      if(thresholdMet(contract.threshold, contract.count)){
        contract.count = 0;
        prepareAd();
        showAd();
      }
    };

    var thresholdMet = function(threshold, count){
      return count >= threshold;
    };

    var prepareAd = function(){
      
    };

    var showAd = function(){
      console.log('SHOW AD');
    };

    return contract;
  });
