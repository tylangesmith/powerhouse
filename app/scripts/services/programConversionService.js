'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.programConversionService
 * @description
 * # programConversionService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('programConversionService', ['utilService', 'unitService', function (utilService, unitService) {
    var contract = {};

    contract.convertPrograms = function(programs){
      return programs.map(function(program){
        if(utilService.isDefined(program.unit)){
          return convertProgram(program);
        }
      });
    };

    var convertProgram = function(program){
      var unit = unitService.getCurrentUnit();

      var convertedProgram = angular.copy(program);

      if(utilService.isDefined(convertedProgram.unit) && convertedProgram.unit !== unit.name){
        convertedProgram.increment = convertIncrement(convertedProgram.unit, convertedProgram.increment);
        convertedProgram.exercises = convertExercises(convertedProgram.unit, convertedProgram.exercises);
        convertedProgram.weeks = convertWeeks(convertedProgram.unit, convertedProgram.weeks);
        convertedProgram.unit = unit.name;
      }

      return convertedProgram;
    };

    var convertIncrement = function(key, increment){
      return unitService.convert(key, increment);
    };

    var convertExercises = function(key, exercises){
      return exercises.map(function(exercise){
        exercise.oneRepMax = unitService.convert(key, exercise.oneRepMax);
        return exercise;
      });
    };

    var convertWeeks = function(key, weeks){
      return weeks.map(function(week){
        week.days = week.days.map(function(day){
          day.sets = day.sets.map(function(set){
            set.exercises = set.exercises.map(function(setExercise){
              setExercise.weight = unitService.convert(key, setExercise.weight);
              return setExercise;
            });
            return set;
          });
          return day;
        });
        return week;
      }); 
    };

    return contract;
  }]);
