'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.exerciseTypeService
 * @description
 * # exerciseTypeService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('exerciseTypeService', ['utilService', function (utilService) {
    
    var contract = {};
    
    var exerciseTypes = [
      { id: 0, name: 'Weighted' },
      { id: 1, name: 'Non-weighted' },
      { id: 2, name: 'Cardio' }
    ];

    contract.getExerciseTypes = function(){
      return exerciseTypes;
    };

    contract.getExerciseTypeById = function(id){
      if(utilService.isNumber(id)){
        var fId = utilService.getNumber(id);
        return exerciseTypes.find(function(exerciseType){
          return exerciseType.id === fId;
        });
      }
    };

    contract.getExerciseTypeByName = function(name){
      return exerciseTypes.find(function(exerciseType){
        return exerciseType.name.toLowerCase() === name.toString().toLowerCase();
      });
    };

    contract.exercisesSame = function(exerciseOne, exerciseTwo){
      return (exerciseOne.id === exerciseTwo.id && exerciseOne.name === exerciseTwo.name && exerciseOne.exerciseType.id === exerciseTwo.exerciseType.id);
    };

    contract.containsExercise = function(exercises, exercise){
      for(var i = 0; i < exercises.length; i++){
        if(exercises[i].id === exercise.id){
          return true;
        }
      }
      return false;
    };

    return contract;
  }]);
