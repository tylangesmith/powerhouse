'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.storageService
 * @description
 * # storageService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('storageService', ['utilService', 'localStorageService', function (utilService, localStorageService) {
    var contract = {};

    contract.storeValue = function(key, value){
      localStorageService.set(key, value);
    };

    contract.getValue = function(key){
      return localStorageService.get(key);
    };

    contract.getValueOrDefault = function(key, dValue){
      var value = localStorageService.get(key);
      if(utilService.isDefined(value)){
        return value;
      }
      return dValue;    
    };

    contract.reset = function(){
      localStorageService.clearAll();
    };

    return contract;
  }]);
