'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramTypeService
 * @description
 * # addProgramTypeService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramTypeService', ['utilService', 'programTypeService', 'exerciseTypeService', function (utilService, programTypeService, exerciseTypeService) {
    
    var contract = {
      exercises: [],
      weeks: [],
      previousWeeks: []
    };
    
    contract.addProgramType = function(programTypeName, level, description, exercises, weeks){
      programTypeService.addProgramType(programTypeName, level, description, exercises, weeks);
    } ;

    contract.isInvalid = function(programName, level, description, exercises, weeks){
      return (isNameInvalid(programName) || isLevelInvalid(level) || isDescriptionInvalid(description) || isExerciseInvalid(exercises) || isWeeksInvalid(weeks));
    };

    contract.exerciseRemoved = function(exercises, weeks){
      var rWeeks = weeks;
      contract.previousWeeks = angular.copy(weeks);

      var updatedWeeks = [];
      rWeeks.forEach(function(week){
        var updatedDays = updateDays(exercises, week);
        if(updatedDays.length > 0){
          updatedWeeks.push(week);
        }
      });

      return updatedWeeks;
    };

    var updateDays = function(exercises, week){
      var updatedDays = [];

      week.days.forEach(function(day){
        var updatedSets = updateSets(exercises, day);
        if(updatedSets.length > 0){
          updatedDays.push(day);
        }
      });

      return updatedDays;
    };

    var updateSets = function(exercises, day){
      var updatedSets = [];

      day.sets.forEach(function(set){
        var updatedSetExercises = updateSetExercises(exercises, set);
        if(updatedSetExercises.length > 0){
          updatedSets.push(set);
        }
      });

      return updatedSets;
    };

    var updateSetExercises = function(exercises, set){
      var updatedSetExercises = [];

      set.exercises.forEach(function(setExercise){
        if(exerciseTypeService.containsExercise(exercises, setExercise.exercise) === true){
          updatedSetExercises.push(setExercise);
        }
      });

      return updatedSetExercises;
    };

    contract.exerciseRemoveUndone = function(){
      contract.weeks = contract.previousWeeks;
    };

    var isNameInvalid = function(programName){
      return (utilService.isUndefined(programName) || programName === '' || programTypeService.nameTaken(programName));
    };

    var isLevelInvalid = function(level){
      return utilService.isUndefined(level);
    };

    var isDescriptionInvalid = function(description){
      return utilService.isUndefined(description);
    };

    var isExerciseInvalid = function(exercises){
      return (utilService.isUndefined(exercises) || exercises.length <= 0);
    };

    var isWeeksInvalid = function(weeks){
      return (utilService.isUndefined(weeks) || weeks.length <= 0);
    };

    return contract;
  }]);
