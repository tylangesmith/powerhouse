'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.programTypeLevelService
 * @description
 * # programTypeLevelService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('programTypeLevelService', function () {
    var contract = {};
    
    var levels = [
      { id: 0, name: 'Beginner' },
      { id: 1, name: 'Intermediate' },
      { id: 2, name: 'Expert' }
    ];

    contract.getLevels = function(){
      return levels;
    };

    contract.getDefault = function(){
      return levels[0];
    };

    contract.getLevelById = function(id){
      for(var i = 0; i < levels.length; i++){
        var level = levels[i];
        if(level.id === id){
          return level; 
        }
      }     
    };

    return contract;
  });
