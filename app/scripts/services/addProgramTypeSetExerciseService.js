'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramTypeSetExerciseService
 * @description
 * # addProgramTypeSetExerciseService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramTypeSetExerciseService', ['utilService', function (utilService) {
    var contract = {};

    contract.addSetExercise = function(setExerciseArray){
      setExerciseArray.push(generateSetExercise(utilService.getUniqueId(setExerciseArray)));
      return setExerciseArray;
    };

    contract.removeSetExercise = function(setExerciseArray, setExercise){
      return utilService.removeFromArray(setExerciseArray, setExercise);
    };

    var generateSetExercise = function(id){
      return {
        id: id,
        confirmed: false,
        exercise: {},
      };
    };

    return contract;
  }]);
