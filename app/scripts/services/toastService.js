'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.toastService
 * @description
 * # toastService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('toastService', ['$mdToast', function ($mdToast) {
    var contract = {};

    var position = 'bottom';

    contract.showUndoToast = function(text, undoFunction){
      $mdToast.show(getUndoToastObject(text, position)).then(function(response){
        if (response === 'ok'){
          undoFunction();
        }
      });
    };

    contract.showProgramCompleteToast = function(text, completeFunction){
      $mdToast.show(getProgramCompleteObject(text, position)).then(function(){
        completeFunction();
      });
    };

    var getUndoToastObject = function(text, position){
      return $mdToast.simple()
        .textContent(text)
        .action('UNDO')
        .highlightAction(true)
        .position(position);
    };

    var getProgramCompleteObject = function(text, position){
      return $mdToast.simple()
        .textContent(text)
        .position(position);
    };

    return contract;
  }]);
