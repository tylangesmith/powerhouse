'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.utilService
 * @description
 * # utilService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('utilService', function () {
      var contract = {};

      contract.isDefined = function(value){
        return (angular.isDefined(value) && value !== null);
      };

      contract.isUndefined = function(value){
        return !contract.isDefined(value);
      };

      contract.isNumber = function(value){
        // Ensure our value is defined
        if(contract.isUndefined(value)){
          return false;
        }
        return !isNaN(value);
      };

      contract.getNumber = function(value){
        if(!contract.isNumber(value)){
          return -1;
        }
        return parseFloat(value);
      };

      contract.getUniqueId = function(values){
        var id = 0;
        values.map(function(value){
          // Check if the value is >= to the current highest id
          if(value.hasOwnProperty('id') && value.id >= id){
            id = (value.id + 1);
          }
        });
        return id;
      };

      contract.removeFromArray = function(array, value){
        // Find index
        var index = array.indexOf(value);

        if(index !== -1){
          array.splice(index, 1);
        }
        return array;
      };

      contract.round = function(number, dp, increments){
        return (Math.round(((contract.getNumber(number)) / contract.getNumber(increments))) * contract.getNumber(increments)).toFixed(contract.getNumber(dp));
      };

      contract.getValueOrDefault = function(value, dValue){
        if(contract.isUndefined(value)){
          return dValue;
        }
        return value;
      };

      return contract;
  });
