'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.programTypeService
 * @description
 * # programTypeService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('programTypeService', ['utilService', 'storageService', 'keyHandlerService', 'toastService', 'defaultProgramTypeService', 
  function (utilService, storageService, keyHandlerService, toastService, defaultProgramTypeService) {
    
    var contract = {
      programTypes: []
    };

    var init = function(){
      contract.programTypes = defaultProgramTypeService.getDefaultProgramTypes().concat(getProgramTypesFromStorage());
    };

    contract.reset = function(){
      contract.programTypes = defaultProgramTypeService.getDefaultProgramTypes();
      storeProgramTypes();
    };

    contract.addProgramType = function(programTypeName, level, description, exercises, weeks){
      var programType = generateProgramType(programTypeName, level, description, exercises, weeks);
      contract.programTypes.push(programType);
      console.log(angular.toJson(programType));
      storeProgramTypes();
    };

    contract.removeProgramType = function(programType){
      var tempProgramTypes = angular.copy(contract.programTypes);
      
      toastService.showUndoToast('Program type removed', function(){
        contract.programTypes = tempProgramTypes;  
        storeProgramTypes();
      });

      contract.programTypes = utilService.removeFromArray(contract.programTypes, programType);
      removeProgramType(programType);
    };

    contract.getProgramTypes = function(){
      return contract.programTypes;
    };

    contract.nameTaken = function(programTypeName){
      for(var i = 0; i < contract.programTypes.length; i++){
        if(utilService.isDefined(contract.programTypes[i].programTypeName) && contract.programTypes[i].programTypeName === programTypeName){
          return true;
        }
      }
      return false;
    };

    contract.getProgramType = function(id){
      return contract.programTypes.find(function(programType){
        if(programType.id === parseInt(id)){
          return programType;
        }
      });
    };

    contract.containsProgramType = function(programTypes, programType){
      return (programTypes.findIndex(function(pType){
        return pType.id === programType.id;
      }) !== -1);
    };

    contract.validProgramType = function(programType){
      return (utilService.isDefined(programType) && utilService.isDefined(programType.id) && 
      utilService.isDefined(programType.totalNumberOfSets) && utilService.isDefined(programType.programTypeName) && 
      utilService.isDefined(programType.exercises) && utilService.isDefined(programType.weeks));
    };
    
    var removeProgramType = function(programType){
      contract.programTypes = utilService.removeFromArray(contract.programTypes, programType);
      storeProgramTypes();
    };

    var storeProgramTypes = function(){
      storageService.storeValue(keyHandlerService.keys.programType, getNonDefaultProgramTypes(contract.programTypes));
    };

    var getNonDefaultProgramTypes = function(programTypes){
      return programTypes.filter(function(programType){
        return (utilService.isUndefined(programType.default) || programType.default === false);
      });
    };

    var getProgramTypesFromStorage = function(){
      return storageService.getValueOrDefault(keyHandlerService.keys.programType, []);
    };

    var generateProgramType = function(programTypeName, level, description, exercises, weeks){
      return {
        id: utilService.getUniqueId(contract.programTypes),
        description: description,
        level: level,
        totalNumberOfSets: calculateNumberOfSets(weeks),
        programTypeName: programTypeName,
        exercises: exercises,
        weeks: weeks,
        default: false
      };
    };

    var calculateNumberOfSets = function(weeks){
      var totalSets = 0;

      weeks.forEach(function(week){
        week.days.forEach(function(day){
          day.sets.forEach(function(set){
            if(utilService.isDefined(set.numberOfSets) && utilService.isNumber(set.numberOfSets)){
              totalSets += set.numberOfSets;
            }
          });
        });
      });

      return totalSets;
    };

    init();
    return contract;
  }]);
