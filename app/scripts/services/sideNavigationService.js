'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.sideNavigationService
 * @description
 * # sideNavigationService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('sideNavigationService', ['$mdSidenav', function ($mdSidenav) {
    var contract = {};

    contract.toggleSidenav = function(id){
      $mdSidenav(id).toggle();
    };

    return contract;
  }]);
