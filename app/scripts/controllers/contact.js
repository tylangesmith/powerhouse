'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('ContactCtrl', ['$scope', function ($scope) {
    $scope.email = 'tylangesmith1995@gmail.com';
  }]);