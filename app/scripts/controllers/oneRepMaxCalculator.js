'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:OnerepmaxcalculatorCtrl
 * @description
 * # OnerepmaxcalculatorCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('OneRepMaxCalculatorCtrl', ['$scope', 'oneRepMaxCalculatorService', function ($scope, oneRepMaxCalculatorService) {
    var init = function(){
      oneRepMaxCalculatorService.reset();
      $scope.weightLifted = '';
      $scope.numberOfReps = '';
      $scope.oRMPercentages = oneRepMaxCalculatorService.getORMPercentages();
      $scope.unit = oneRepMaxCalculatorService.getUnit();
      $scope.helpOneRepMaxCalculatorUrl = oneRepMaxCalculatorService.helpOneRepMaxCalculatorUrl;
    };

    $scope.$watchGroup([
      'weightLifted',
      'numberOfReps'
    ], function(newValues, oldValues){
      if(newValues !== oldValues){
        oneRepMaxCalculatorService.calculateORMPercentages(newValues[0], newValues[1]);
      }
    });

    $scope.$watch(function(){
      return oneRepMaxCalculatorService.getUnit();
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        $scope.unit = oneRepMaxCalculatorService.getUnit();
      }
    });

    $scope.$watchCollection(function(){
      return oneRepMaxCalculatorService.getORMPercentages();
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        $scope.oRMPercentages = oneRepMaxCalculatorService.getORMPercentages();
      }
    });

    init();
  }]);
