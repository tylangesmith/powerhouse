'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('dashboardCtrl', ['$scope', 'dashboardService', function ($scope, dashboardService) {

    // Completed Program Data
    $scope.completedHeaderText = dashboardService.completedHeaderText;
    $scope.completedHighlightText = dashboardService.completedHighlightText;
    $scope.completedHighlightColor = dashboardService.completedHighlightColor;
    $scope.completeSubheadText = dashboardService.completeSubheadText;
    
    // Active Program Data
    $scope.activeHeaderText = dashboardService.activeHeaderText;
    $scope.activeHighlightText = dashboardService.activeHighlightText;
    $scope.activeHighlightColor = dashboardService.activeHighlightColor;
    $scope.activeSubheadText = dashboardService.activeSubheadText;

    // Quick Complete
    $scope.quickCompleteProgram = dashboardService.getQuickCompleteProgram();

    // Program List
    $scope.programs = dashboardService.getActivePrograms(); 

    $scope.$watchCollection(function(){
      return dashboardService.getCompletedPrograms();
    }, function(){
      $scope.completedHighlightText = dashboardService.completedHighlightText;
    });

    $scope.$watchCollection(function(){
      return dashboardService.getActivePrograms();
    }, function(newValue, oldValue){
      $scope.activeHighlightText = dashboardService.activeHighlightText;
      if(newValue !== oldValue){
        $scope.programs = dashboardService.getActivePrograms();
      }
    });

    $scope.$watch(function(){
      return dashboardService.getQuickCompleteProgram();
    }, function(){
      $scope.quickCompleteProgram = dashboardService.getQuickCompleteProgram();
    }, true);
  }]);
