'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:AddprogramCtrl
 * @description
 * # AddprogramCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('addProgramCtrl', ['$scope', '$location', 'addProgramService', function ($scope, $location, addProgramService) {
        $scope.programName = '';
        $scope.programType = '';
        $scope.increment = '';
        $scope.exercises = [];

        $scope.addFunction = function(programName, programType, increment, exercises){
          addProgramService.addProgram(programName, programType, increment, exercises);
          $location.path('program-list');
        };

        $scope.removeFunction = function(){
          $scope.programName = '';
          $scope.programType = '';
          $scope.increment = '';
          $scope.exercises = [];
        };

        $scope.invalidFunction = function(programName, programType, increment, exercises){
          return addProgramService.invalid(programName, programType, increment, exercises);
        };

        $scope.$watch(function(){
          return $scope.programType;
        },
        function(newValue, oldValue){
          if(newValue !== oldValue && newValue !== ''){
            $scope.exercises = addProgramService.getExercises($scope.programType);
          }
        });
  }]);
