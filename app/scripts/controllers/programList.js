'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:ProgramlistCtrl
 * @description
 * # ProgramlistCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('programListCtrl', ['$scope', '$filter', 'programService', 'programListService', function ($scope, $filter, programService, programListService) {

    var updatePrograms = function(){
      $scope.originalPrograms = programService.getPrograms();
      $scope.filteredPrograms = $scope.originalPrograms;
      $scope.programs = $scope.originalPrograms;
    };

    // Empty list
    $scope.emptyListMessage = 'Start by adding a program';
    $scope.emptyListButtonText = 'Add Program';
    $scope.emptyListButtonLink = '#/add-program';

    $scope.filterProperty = 'name';

    $scope.orderValues = programListService.getOrderValues();
    $scope.orderKey = programListService.orderKey;
    $scope.reverseKey = programListService.reverseKey;

    $scope.$watchCollection(function(){
      return programService.getPrograms();
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        updatePrograms();
      }
    });
    
    updatePrograms();
  }]);
