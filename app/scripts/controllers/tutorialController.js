'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:TutorialcontrollerCtrl
 * @description
 * # TutorialcontrollerCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('tutorialController', ['$scope', '$mdDialog', 'storageService', 'keyHandlerService', function ($scope, $mdDialog, storageService, keyHandlerService) {
    $scope.ariaLabel = 'Tutorial';
    $scope.hideDialog = function(){
      $mdDialog.hide();
      storageService.storeValue(keyHandlerService.keys.tutorialComplete, true);
    };
  }]);
