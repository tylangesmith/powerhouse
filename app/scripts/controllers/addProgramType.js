'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:AddprogramtypeCtrl
 * @description
 * # AddprogramtypeCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('addProgramTypeCtrl', ['$scope', '$location', 'addProgramTypeService', 'toastService', 'programTypeLevelService', function ($scope, $location, addProgramTypeService, toastService, programTypeLevelService) {

    $scope.programTypeName = '';
    $scope.level = programTypeLevelService.getDefault();
    $scope.description = '';
    $scope.exercises = [];
    $scope.weeks = [];

    $scope.addFunction = function(programTypeName, level, description, exercises, weeks){
      addProgramTypeService.addProgramType(programTypeName, level, description, exercises, weeks);
      $location.path('program-type-list');
    };

    $scope.removeFunction = function(){
      var tempProgramTypeName = angular.copy($scope.programTypeName);
      var tempProgramLevel = angular.copy($scope.level);
      var tempProgramDescription = angular.copy($scope.description);
      var tempExercises = angular.copy($scope.exercises);
      var tempWeeks = angular.copy($scope.weeks);
      toastService.showUndoToast('Program type removed', function(){
        $scope.programTypeName = tempProgramTypeName;
        $scope.level = tempProgramLevel;
        $scope.description = tempProgramDescription;
        $scope.exercises = tempExercises;
        $scope.weeks = tempWeeks;
      });

      $scope.programTypeName = '';
      $scope.level = programTypeLevelService.getDefault();
      $scope.description = '';
      $scope.exercises = [];
      $scope.weeks = [];
    };

    $scope.invalidFunction = function(programName, level, description, exercises, weeks){
      return addProgramTypeService.isInvalid(programName, level, description, exercises, weeks);
    };

    $scope.$watchCollection(function(){
      return $scope.exercises;
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        addProgramTypeService.exercises = newValue;
      }
    });
  }]);
