'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:EditprogramCtrl
 * @description
 * # EditprogramCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('EditProgramCtrl', ['$scope', '$routeParams', '$location', '$window', 'programService', 'addProgramService', 'utilService', function ($scope, $routeParams, $location, $window, programService, addProgramService, utilService) {

    $scope.program = programService.getProgram($routeParams.id);

    $scope.programName = angular.copy($scope.program.name);
    $scope.programType = angular.copy($scope.program.programType);
    $scope.increment = angular.copy(utilService.getNumber($scope.program.increment));
    $scope.exercises = angular.copy(addProgramService.getExercisesWithORM($scope.programType, $scope.program));

    $scope.addFunction = function(programName, programType, increment, exercises){
      addProgramService.editProgram($scope.program, $scope.program.id, programName, programType, increment, exercises);
      $window.history.back();
    };

    $scope.removeFunction = function(){
      $window.history.back();
    };

    $scope.invalidFunction = function(programName, programType, increment, exercises){
      return addProgramService.invalidAllowDuplicateNames(programName, programType, increment, exercises);
    };

    $scope.$watch(function(){
      return $scope.programType;
    },
    function(newValue, oldValue){
      if(newValue !== oldValue && newValue !== ''){
        $scope.exercises = addProgramService.getExercisesWithORM($scope.programType, $scope.program);
      }
    });

  }]);
