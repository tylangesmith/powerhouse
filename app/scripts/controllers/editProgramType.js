'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:EditprogramtypeCtrl
 * @description
 * # EditprogramtypeCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('EditProgramTypeCtrl', ['$scope', '$routeParams', '$location', '$window', 'utilService', 'programTypeService', 'addProgramTypeService', 'programTypeLevelService', 
  function ($scope, $routeParams, $location, $window,utilService, programTypeService, addProgramTypeService, programTypeLevelService) {
    $scope.programType = programTypeService.getProgramType($routeParams.id);

    $scope.programTypeName = angular.copy($scope.programType.programTypeName);
    $scope.level = angular.copy(utilService.getValueOrDefault($scope.programType.level, programTypeLevelService.getDefault()));
    $scope.description = angular.copy(utilService.getValueOrDefault($scope.programType.description, ''));
    $scope.exercises = angular.copy($scope.programType.exercises);
    $scope.weeks = angular.copy($scope.programType.weeks);

    $scope.addFunction = function(programTypeName, level, description, exercises, weeks){
      addProgramTypeService.addProgramType(programTypeName, level, description, exercises, weeks);
      $location.path('program-type-list');
    };

    $scope.removeFunction = function(){
      $window.history.back();
    };

    $scope.invalidFunction = function(programName, level, description, exercises, weeks){
      return addProgramTypeService.isInvalid(programName, level, description, exercises, weeks);
    };

    $scope.$watchCollection(function(){
      return $scope.exercises;
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        addProgramTypeService.exercises = newValue;
      }
    });

  }]);
