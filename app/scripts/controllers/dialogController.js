'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:DialogcontrollerCtrl
 * @description
 * # DialogcontrollerCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('dialogController', ['$scope', '$mdDialog', function ($scope, $mdDialog) {
    $scope.ariaLabel = 'Dialog';
    $scope.hideDialog = function(){
      $mdDialog.hide();
    };
  }]);
