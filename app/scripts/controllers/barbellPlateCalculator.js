'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:BarbellplatecalculatorCtrl
 * @description
 * # BarbellplatecalculatorCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('BarbellPlateCalculatorCtrl', ['$scope', 'barbellPlateCalculatorService', function ($scope, barbellPlateCalculatorService) {

    $scope.targetWeight = '';
    barbellPlateCalculatorService.calculatePlates($scope.targetWeight);
    $scope.currentWeights = barbellPlateCalculatorService.getCurrentWeights();
    $scope.barbellWeight = barbellPlateCalculatorService.getBarbellWeight();
    $scope.unit = barbellPlateCalculatorService.getCurrentUnit();
    $scope.range = barbellPlateCalculatorService.getRange();
    $scope.plates = barbellPlateCalculatorService.getPlates();

    $scope.helpUrl = barbellPlateCalculatorService.helpUrl;

    // Watch the target weight 
    $scope.$watch(function(){
      return $scope.targetWeight;
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        barbellPlateCalculatorService.calculatePlates($scope.targetWeight);
      }
    });

    $scope.$watch(function(){
      return $scope.barbellWeight;
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        barbellPlateCalculatorService.updateBarbellWeight($scope.barbellWeight);
        barbellPlateCalculatorService.calculatePlates($scope.targetWeight);
      }
    });

    $scope.$watch(function(){
      return $scope.currentWeights;
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        barbellPlateCalculatorService.updateCurrentWeights($scope.currentWeights);
        barbellPlateCalculatorService.calculatePlates($scope.targetWeight);
      }
    }, true);

    $scope.$watch(function(){
      return barbellPlateCalculatorService.getPlates();
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        $scope.plates = barbellPlateCalculatorService.getPlates();
      }
    }, true);
  }]);
