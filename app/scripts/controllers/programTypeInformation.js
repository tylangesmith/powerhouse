'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:ProgramtypeinformationCtrl
 * @description
 * # ProgramtypeinformationCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('ProgramTypeInformationCtrl', ['$scope', '$routeParams', '$location', 'programTypeInformationService', 'programTypeService', function ($scope, $routeParams, $location, programTypeInformationService, programTypeService) {

    $scope.programType = programTypeService.getProgramType($routeParams.id);

    $scope.editFunction = function(programType){
      $location.path('edit-program-type/' + programType.id);
    };

    $scope.removeFunction = function(programType){
      programTypeService.removeProgramType(programType);
      $location.path('program-type-list');
    };
    
  }]);
