'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:ProgramtypelistCtrl
 * @description
 * # ProgramtypelistCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('ProgramTypeListCtrl', ['$scope', '$filter', 'programTypeService', 'programTypeListService', function ($scope, $filter, programTypeService, programTypeListService) {

    var updateProgramTypes = function(){
      $scope.originalProgramTypes = programTypeService.getProgramTypes();
      $scope.filteredProgramTypes = $scope.originalProgramTypes;
      $scope.programTypes = $scope.originalProgramTypes;
    };

    // Empty list
    $scope.emptyListMessage = 'Start by adding a program type';
    $scope.emptyListButtonText = 'Add Program Type';
    $scope.emptyListButtonLink = '#/add-program-type';

    $scope.filterProperty = 'programTypeName';

    $scope.orderValues = programTypeListService.getOrderValues();
    $scope.orderKey = programTypeListService.orderKey;
    $scope.reverseKey = programTypeListService.reverseKey;

    $scope.$watchCollection(function(){
      return programTypeService.getProgramTypes();
    }, function(newValue, oldValue){
      if(newValue !== oldValue){
        updateProgramTypes();
      }
    });

    updateProgramTypes();
  }]);
