'use strict';

/**
 * @ngdoc function
 * @name powerHouseApp.controller:PrograminformationCtrl
 * @description
 * # PrograminformationCtrl
 * Controller of the powerHouseApp
 */
angular.module('powerHouseApp')
  .controller('ProgramInformationCtrl', ['$scope', '$routeParams', 'programService', '$location', 'unitService', function ($scope, $routeParams, programService, $location, unitService) {
    $scope.program = programService.getProgram($routeParams.id);

    $scope.unit = unitService.getCurrentUnit();

    $scope.editFunction = function(program){
      $location.path('edit-program/' + program.id);
    };

    $scope.removeFunction = function(program){
      programService.removeProgram(program);
      $location.path('program-list');
    };

    $scope.calculatePercentageComplete = function(day){
      $scope.program = programService.updateProgramComplete($scope.program, day);
    };

  }]);
