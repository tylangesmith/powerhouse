'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.quickCompleteService
 * @description
 * # quickCompleteService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('quickCompleteService', ['utilService', 'unitService', 'programService', function (utilService, unitService, programService) {
    var contract = {
      helpTemplateUrl: 'scripts/directives/quickComplete/quickCompleteHelpTemplate.html',
    };

    contract.defined = function(quickCompleteProgram){
      return (utilService.isDefined(quickCompleteProgram) && !angular.equals(quickCompleteProgram, {}));
    };

    contract.getNextSetData = function(quickCompleteProgram){
      var nextSet = findNextSet(quickCompleteProgram);
      return {
        week: nextSet.week,
        day: nextSet.day,
        set: nextSet.set,
        exercises: nextSet.set.exercises
      };
    };

    contract.updateProgramComplete = function(program, day){
      programService.updateProgramComplete(program, day);
    };

    var findNextSet = function(program){
      for(var i = 0; i < program.weeks.length; i++){
        var week = program.weeks[i];
        for(var y = 0; y < week.days.length; y++){
          var day = week.days[y];
          for(var z = 0; z < day.sets.length; z++){
            var set = day.sets[z];
            if(set.complete === false){
              return {                
                week: week,
                day: day,
                set: set
              };
            }
          }
        }
      }
    };

    return contract;
  }]);
