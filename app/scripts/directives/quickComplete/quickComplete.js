'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:quickComplete
 * @description
 * # quickComplete
 */
angular.module('powerHouseApp')
  .directive('quickComplete', ['utilService', 'quickCompleteService', '$location', function (utilService, quickCompleteService, $location) {
    return {
      templateUrl: 'scripts/directives/quickComplete/quickCompleteView.html',
      restrict: 'E',
      scope: {
        quickCompleteProgram: '='
      },
      link: function postLink(scope) {

        scope.helpTemplateUrl = quickCompleteService.helpTemplateUrl;
        scope.buttonText = 'Programs';

        scope.defined = function(){
          return quickCompleteService.defined(scope.quickCompleteProgram);
        };

        scope.calculatePercentageComplete = function(day){
          quickCompleteService.updateProgramComplete(scope.quickCompleteProgram, day);
        };

        scope.buttonClicked = function(){
          $location.path('program-list');
        };

        scope.$watch(function(){
          return scope.quickCompleteProgram;
        }, function(){
          if(scope.defined(scope.quickCompleteProgram) === true){
            var data = quickCompleteService.getNextSetData(scope.quickCompleteProgram);
            scope.week = data.week;
            scope.day = data.day;
            scope.set = data.set;
            scope.exercises = data.exercises;
          }
        }, true);
      }
    };
  }]);
