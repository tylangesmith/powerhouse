'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:list
 * @description
 * # list
 */
angular.module('powerHouseApp')
  .directive('list', function () {
    return {
      templateUrl: 'scripts/directives/list/listView.html',
      restrict: 'E',
      scope: {
        editFunction: '=',
        removeFunction: '=',
        values: '='
      },
      link: function postLink() {
      }
    };
  });
