'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.helpService
 * @description
 * # helpService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('helpService', ['$mdDialog', function ($mdDialog) {
    var contract = {};

    contract.display = function(templateUrl){
      $mdDialog.show({
        controller: 'dialogController',
        templateUrl: templateUrl,
        ariaLabel: 'Help Dialog',
        clickOutsideToClose: true
      });
    };

    return contract;
  }]);
