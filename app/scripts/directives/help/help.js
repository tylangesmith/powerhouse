'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:help
 * @description
 * # help
 */
angular.module('powerHouseApp')
  .directive('help', ['helpService', function (helpService) {
    return {
      templateUrl: 'scripts/directives/help/helpView.html',
      restrict: 'E',
      scope: {
        templateUrl: '='
      },
      link: function postLink(scope) {
        scope.display = function(){
          helpService.display(scope.templateUrl);
        };
      }
    };
  }]);
