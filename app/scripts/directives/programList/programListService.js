'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.programListService
 * @description
 * # programListService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('programListService', ['unitService', 'keyHandlerService', 'orderListService', function (unitService, keyHandlerService, orderListService) {
    var contract = {
      orderKey: keyHandlerService.keys.programListOrderKey,
      reverseKey: keyHandlerService.keys.programListReversedKey,
      orderValues: [
        {
          prop: 'name',
          text: 'Name'
        },
        {
          prop: 'complete',
          text: 'Complete'
        },
        {
          prop: 'increment',
          text: 'Increment'
        },
        {
          prop: 'percentComplete',
          text: 'Percent Complete'
        },
        {
          prop: 'programType',
          text: 'Experience Level',
          comparator: function(v1, v2){
            if(v1.type === 'object' && v2.type === 'object'){
              var rValue = 0;
              
              if(v1.value.level.id < v2.value.level.id){
                rValue = -1;
              }
              else if(v1.value.level.id > v2.value.level.id){
                rValue = 1;
              }

              return rValue;
            }
          }
        },
      ]
    };

    contract.getOrderValues = function(){
      return orderListService.getOrderByValues(contract.orderValues);
    };

    contract.formatPrograms = function(programs){
      return programs.map(function(program){
        return formatProgram(program);
      });
    };

    var formatProgram = function(program){
      return {
        program: program,
        id: program.id,
        text: program.name,
        secondText: 'Total Weeks: ' + program.weeks.length,
        thirdText: 'Increment: ' + program.increment + unitService.getCurrentUnit().textName,
        percentage: program.percentComplete,
        href: '#/program-information/' + program.id,
        removable: !program.default,
        editable: true,
        color: program.complete ? 'green-50' : 'grey-A100'
      };
    };

    return contract;
  }]);
