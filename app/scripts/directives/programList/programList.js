'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:programList
 * @description
 * # programList
 */
angular.module('powerHouseApp')
  .directive('programList', ['$location', 'programListService', 'programService', function ($location, programListService, programService) {
    return {
      templateUrl: 'scripts/directives/programList/programListView.html',
      restrict: 'E',
      scope: {
        programs: '=',
      },
      link: function postLink(scope) {

        scope.formattedPrograms = programListService.formatPrograms(scope.programs);

        scope.editFunction = function(program){
          $location.path('edit-program/' + program.id);
        };

        scope.removeFunction = function(program){
          programService.removeProgram(program.program);
        };

        scope.$watch(function(){
          return scope.programs;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.formattedPrograms = programListService.formatPrograms(scope.programs);
          }
        }, true);
      }
    };
  }]);
