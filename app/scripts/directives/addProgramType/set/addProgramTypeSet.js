'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeSet
 * @description
 * # addProgramTypeSet
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeSet', ['addProgramTypeSetService', 'addProgramTypeService', function (addProgramTypeSetService, addProgramTypeService) {
    return {
      templateUrl: 'scripts/directives/addProgramType/set/addProgramTypeSetView.html',
      restrict: 'E',
      scope: {
        programTypeSets: '=',
      },
      link: function postLink(scope) {

        scope.remove = addProgramTypeSetService.remove;
        scope.edit = addProgramTypeSetService.edit;
        scope.duplicate = addProgramTypeSetService.duplicate;
        scope.move = addProgramTypeSetService.move;

        scope.helpAddProgramTypeIncrementUrl = addProgramTypeSetService.helpAddProgramTypeIncrementUrl;
        scope.helpAddProgramTypeOneRepMaxUrl = addProgramTypeSetService.helpAddProgramTypeOneRepMaxUrl;

        scope.exercises = addProgramTypeService.exercises;

        scope.addSet = function(){
          scope.programTypeSets = addProgramTypeSetService.addSet(scope.programTypeSets);
        };

        scope.editSet = function(set){
          set.confirmed = false;
        };

        scope.duplicateSet = function(set){
          scope.programTypeSets = addProgramTypeSetService.duplicateSet(scope.programTypeSets, set);
        };

        scope.moveSet = function(set, direction){
          scope.programTypeSets = addProgramTypeSetService.moveSet(scope.programTypeSets, set, direction);
        };

        scope.confirmSet = function(set){
          set.confirmed = true;
          set.exercises.forEach(function(setExercise){
            if(setExercise.exercise.exerciseType.id === 2){
              setExercise = addProgramTypeSetService.formatCardio(setExercise);
            }
          });
        };

        scope.removeSet = function(set){
          scope.programTypeSets = addProgramTypeSetService.removeSet(scope.programTypeSets, set);
        };

        scope.hasConfirmed = function(){
          return addProgramTypeSetService.hasConfirmed(scope.programTypeSets);
        };

        scope.isInvalid = function(set){
          return addProgramTypeSetService.isInvalid(set);
        };

        scope.$watchCollection(function(){
          return addProgramTypeSetService.sets;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.programTypeSets = addProgramTypeSetService.sets;
          }
        });

        scope.$watchCollection(function(){
          return addProgramTypeService.exercises;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.exercises = addProgramTypeService.exercises;
          }
        });
      }
    };
  }]);