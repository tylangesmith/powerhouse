'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramTypeSetTypeNormalService
 * @description
 * # addProgramTypeSetTypeNormalService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramTypeSetTypeNormalService', ['addProgramTypeSetExerciseService', function (addProgramTypeSetExerciseService) {
    var contract = {};

    contract.initSetExercise = function(setExercises){
      if(setExercises.length > 0){
        return setExercises;
      }
      return addProgramTypeSetExerciseService.addSetExercise(setExercises);
    };

    return contract;
  }]);
