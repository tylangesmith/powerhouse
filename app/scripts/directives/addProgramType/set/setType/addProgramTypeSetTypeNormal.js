'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeSetTypeNormal
 * @description
 * # addProgramTypeSetTypeNormal
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeSetTypeNormal', ['addProgramTypeSetTypeNormalService', function (addProgramTypeSetTypeNormalService) {
    return {
      templateUrl: 'scripts/directives/addProgramType/set/setType/addProgramTypeSetTypeNormalView.html',
      restrict: 'E',
      scope: {
        exercises: '=',
        set: '='
      },
      link: function postLink(scope) {

        var init = function(){
          scope.set.exercises = addProgramTypeSetTypeNormalService.initSetExercise(scope.set.exercises);
        };

        init();
      }
    };
  }]);
