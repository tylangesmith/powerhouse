'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramSetTypeSupersetService
 * @description
 * # addProgramSetTypeSupersetService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramTypeSetTypeSupersetService', ['utilService', 'addProgramTypeSetService', 'addProgramTypeSetExerciseService', 'toastService', function (utilService, addProgramTypeSetService, addProgramTypeSetExerciseService, toastService) {
    var contract = {
      remove: true,
      edit: true,
      duplicate: true,
      move: true,
      superSets: []
    };

    contract.initSetExercise = function(setExercises){
      return setExercises.filter(function(setExercise){
        return setExercise.confirmed === true;
      });
    };

    contract.addSuperset = function(setExercises){
      return addProgramTypeSetExerciseService.addSetExercise(setExercises);
    };

    contract.removeSuperset = function(setExercises, setExercise){
      var tempExerciseSetsArray = angular.copy(setExercises);
      toastService.showUndoToast('Superset removed', function(){
        contract.superSets = tempExerciseSetsArray;
      });

      return addProgramTypeSetExerciseService.removeSetExercise(setExercises, setExercise);
    };

    contract.duplicateSetExercise = function(setExerciseArray, setExercise){
      var newSetExercise = angular.copy(setExercise);
      newSetExercise.id = utilService.getUniqueId(setExerciseArray);
      setExerciseArray.push(newSetExercise);
      return setExerciseArray;
    };

    contract.moveSetExercise = function(setExerciseArray, setExercise, direction){
      var moveFunctions = {
        up: function(rArray, setExercise, index){
          if(index !== -1 && index !== 0){
            rArray[index] = rArray[index - 1];
            rArray[index - 1] = setExercise;
          }
          return rArray;
        },
        down: function(rArray, setExercise, index){
          if(index !== -1 && index !== (rArray.length - 1)){
            rArray[index] = rArray[index + 1];
            rArray[index + 1] = setExercise;
          }
          return rArray;
        }
      };

      var rArray = setExerciseArray;

      var index = setExerciseArray.findIndex(function(element){
        return element.id === setExercise.id;
      });
    
      return moveFunctions[direction](rArray, setExercise, index);
    };

    contract.hasConfirmed = function(setExercises){
      for(var i = 0; i < setExercises.length; i++){
        if(setExercises[i].confirmed === true){
          return true;
        }
      }
      return false;  
    };

    contract.isInvalid = function(setExercise){
      return addProgramTypeSetService.setExerciseTypesInvalid(setExercise);
    };

    return contract;
  }]);
