'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeSetTypeSuperset
 * @description
 * # addProgramTypeSetTypeSuperset
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeSetTypeSuperset', ['addProgramTypeSetTypeSupersetService', function (addProgramTypeSetTypeSupersetService) {
    return {
      templateUrl: 'scripts/directives/addProgramType/set/setType/addProgramTypeSetTypeSupersetView.html',
      restrict: 'E',
      scope: {
        exercises: '=',
        set: '='
      },
      link: function postLink(scope) {

        scope.remove = addProgramTypeSetTypeSupersetService.remove;
        scope.edit = addProgramTypeSetTypeSupersetService.edit;
        scope.duplicate = addProgramTypeSetTypeSupersetService.duplicate;
        scope.move = addProgramTypeSetTypeSupersetService.move;

        var init = function(){
          scope.set.exercises = addProgramTypeSetTypeSupersetService.initSetExercise(scope.set.exercises);
        };

        scope.addSuperset = function(){
          scope.set.exercises = addProgramTypeSetTypeSupersetService.addSuperset(scope.set.exercises);
        };

        scope.editSetExercise = function(setExercise){
          setExercise.confirmed = false;
        };

        scope.duplicateSetExercise = function(setExercise){
          scope.set.exercises = addProgramTypeSetTypeSupersetService.duplicateSetExercise(scope.set.exercises, setExercise);
        };

        scope.moveSetExercise = function(setExercise, direction){
          scope.set.exercises = addProgramTypeSetTypeSupersetService.moveSetExercise(scope.set.exercises, setExercise, direction);
        };

        scope.confirmSetExercise = function(setExercise){
          setExercise.confirmed = true;
        };

        scope.removeSetExercise = function(setExercise){
          scope.set.exercises = addProgramTypeSetTypeSupersetService.removeSuperset(scope.set.exercises, setExercise);
        };

        scope.isInvalid = function(setExercise){
          return addProgramTypeSetTypeSupersetService.isInvalid(setExercise);
        };

        scope.hasConfirmed = function(){
          return addProgramTypeSetTypeSupersetService.hasConfirmed(scope.set.exercises);
        };

        scope.$watch(function(){
          return addProgramTypeSetTypeSupersetService.superSets;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.set.exercises = addProgramTypeSetTypeSupersetService.superSets;
          }
        });

        init();
      }
    };
  }]);
