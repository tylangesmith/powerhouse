'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramTypeSetService
 * @description
 * # addProgramTypeSetService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramTypeSetService', ['utilService', 'toastService', 'setTypeService', function (utilService, toastService, setTypeService) {
    var contract = {
      remove: true,
      edit: true,
      duplicate: true,
      move: true,
      helpAddProgramTypeIncrementUrl: 'scripts/directives/addProgramType/help/helpAddProgramTypeIncrementTemplate.html',
      helpAddProgramTypeOneRepMaxUrl: 'scripts/directives/addProgramType/help/helpAddProgramTypeOneRepMaxTemplate.html',
      sets: []
    };

    contract.addSet = function(setArray){
      setArray.push(generateSet(utilService.getUniqueId(setArray)));
      return setArray;
    };

    contract.removeSet = function(setsArray, set){
      var tempSetsArray = angular.copy(setsArray);
      toastService.showUndoToast('Set removed', function(){
        contract.sets = tempSetsArray;
      });

      return utilService.removeFromArray(setsArray, set);
    };

    contract.duplicateSet = function(setArray, set){
      var newSet = angular.copy(set);
      newSet.id = utilService.getUniqueId(setArray);
      setArray.push(newSet);
      return setArray;
    };

    contract.moveSet = function(setArray, set, direction){
      var moveFunctions = {
        up: function(rArray, set, index){
          if(index !== -1 && index !== 0){
            rArray[index] = rArray[index - 1];
            rArray[index - 1] = set;
          }
          return rArray;
        },
        down: function(rArray, set, index){
          if(index !== -1 && index !== (rArray.length - 1)){
            rArray[index] = rArray[index + 1];
            rArray[index + 1] = set;
          }
          return rArray;
        }
      };

      var rArray = setArray;

      var index = setArray.findIndex(function(element){
        return element.id === set.id;
      });
    
      return moveFunctions[direction](rArray, set, index);
    };

    contract.hasConfirmed = function(setArray){
      for(var i = 0; i < setArray.length; i++){
        if(setArray[i].confirmed === true){
          return true;
        }
      }
      return false;
    };

    contract.formatCardio = function(setExercise){
      var fSetExercise = setExercise;
      fSetExercise = addDuration(setExercise);
      fSetExercise = addReps(setExercise);
      return fSetExercise;
    };

    contract.isInvalid = function(set){
      return (setTypeInvalid(set.setType) || setNumberOfSetsInvalid(set) || setExercisesInvalid(set.setType, set.exercises));
    };

    contract.setExerciseTypesInvalid = function(setExercise){
      var exerciseTypeFunctions = {
        0: weightedExerciseInvalid(setExercise),
        1: nonWeightedExerciseInvalid(setExercise),
        2: cardioExerciseInvalid(setExercise)
      };
      if(exerciseInvalid(setExercise.exercise)){
        return true;
      }
      return exerciseTypeFunctions[setExercise.exercise.exerciseType.id];
    };

    var setNumberOfSetsInvalid = function(set){
      return (utilService.isUndefined(set.numberOfSets) || set.numberOfSets < 0);
    };

    var setTypeInvalid = function(setType){
      return (utilService.isUndefined(setType) || utilService.isUndefined(setType.id));
    };

    var setExercisesInvalid = function(setType, setExercises){
      return (minNumberOfExercises(setType, setExercises) || setExercisesNotConfirmed(setType, setExercises) || 
      setExercisesTypesInvalid(setExercises));
    };

    var minNumberOfExercises = function(setType, setExercises){
      var minNumberOfSupersetsInvalid = function(setType, setExercises){
        if(setType.id === 1){
          return setExercises.length < 2;
        }
        return false;
      };
      return (utilService.isUndefined(setExercises) || setExercises.length < 1 || minNumberOfSupersetsInvalid(setType, setExercises));
    };

    var setExercisesNotConfirmed = function(setType, setExercises){
      if(setType.id === 0){
        return false;
      }
      for(var i = 0; i < setExercises.length; i++){
        if(setExercises[i].confirmed === false){
          return true;
        }
      }
      return false;
    };

    var setExercisesTypesInvalid = function(setExercises){
      // Checks if invalid for weighted, non-weighted and cardio exercise types
      for(var i = 0; i < setExercises.length; i++){
        if(contract.setExerciseTypesInvalid(setExercises[i]) === true){
          return true;
        }
      }
      return false;
    };

    var exerciseInvalid = function(exercise){
      return (utilService.isUndefined(exercise) || utilService.isUndefined(exercise.id));
    };

    var weightedExerciseInvalid = function(setExercise){
      return (utilService.isUndefined(setExercise.numberOfReps) || setExercise.numberOfReps < 1 ||
      utilService.isUndefined(setExercise.oneRepMaxPercentage) || setExercise.oneRepMaxPercentage < 0 || 
      setExercise.oneRepMaxPercentage > 100 || utilService.isUndefined(setExercise.incrementMultiplier) || 
      setExercise.incrementMultiplier < 0);
    };

    var nonWeightedExerciseInvalid = function(setExercise){
      return (utilService.isUndefined(setExercise.numberOfReps) || setExercise.numberOfReps < 1 );
    };

    var cardioExerciseInvalid = function(setExercise){
      return (utilService.isUndefined(setExercise.minutes) || setExercise.minutes < 0 || 
      utilService.isUndefined(setExercise.seconds) || setExercise.seconds < 0 || setExercise.seconds > 59);
    };

    var addDuration = function(setExercise){
      var fSetExercise = setExercise;
      fSetExercise.duration = formatTime(setExercise.minutes, setExercise.seconds);
      return fSetExercise;
    };

    var addReps = function(setExercise){
      var fSetExercise = setExercise;
      fSetExercise.numberOfReps = 1;
      return fSetExercise;
    };

    var formatTime = function(minutes, seconds){
      var rString = '';
      rString += minutes;
      rString += ':';

      if(seconds < 10){
        rString += '0' + seconds;
      }
      else if(seconds !== undefined){
        rString += seconds;
      }
      else{
        rString += '00';
      }

      return rString;
    };

    var generateSet = function(id){
      return {
        id: id,
        exercises: [],
        confirmed: false,
        complete: false,
        setType: setTypeService.getDefault()
      };
    };

    return contract;
  }]);
