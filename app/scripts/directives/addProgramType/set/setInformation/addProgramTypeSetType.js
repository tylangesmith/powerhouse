'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeSetType
 * @description
 * # addProgramTypeSetType
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeSetType', ['setTypeService', function (setTypeService) {
    return {
      templateUrl: 'scripts/directives/addProgramType/set/setInformation/addProgramTypeSetTypeView.html',
      restrict: 'E',
      scope: {
        setType: '='
      },
      link: function postLink(scope) {
        scope.setTypes = setTypeService.types;
      }
    };
  }]);
