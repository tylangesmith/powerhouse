'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeSetExerciseSelect
 * @description
 * # addProgramTypeSetExerciseSelect
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeSetExerciseSelect', function () {
    return {
      templateUrl: 'scripts/directives/addProgramType/set/setInformation/addProgramTypeSetExerciseSelectView.html',
      restrict: 'E',
      scope: {
        exercises: '=',
        exercise: '='
      },
      link: function postLink() {
      }
    };
  });
