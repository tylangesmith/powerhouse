'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeSetNumberOfSets
 * @description
 * # addProgramTypeSetNumberOfSets
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeSetNumberOfSets', function () {
    return {
      templateUrl: 'scripts/directives/addProgramType/set/setInformation/addProgramTypeSetNumberOfSetsView.html',
      restrict: 'E',
      scope: {
        numberOfSets: '='
      },
      link: function postLink() {
      }
    };
  });
