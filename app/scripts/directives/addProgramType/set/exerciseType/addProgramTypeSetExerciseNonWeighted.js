'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeSetExerciseNonWeighted
 * @description
 * # addProgramTypeSetExerciseNonWeighted
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeSetExerciseNonWeighted', function () {
    return {
      templateUrl: 'scripts/directives/addProgramType/set/exerciseType/addProgramTypeSetExerciseNonWeightedView.html',
      restrict: 'E',
      scope: {
        setExercise: '='
      },      
      link: function postLink() {
      }
    };
  });
