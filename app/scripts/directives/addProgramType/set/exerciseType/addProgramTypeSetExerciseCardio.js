'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeSetExerciseCardio
 * @description
 * # addProgramTypeSetExerciseCardio
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeSetExerciseCardio', function () {
    return {
      templateUrl: 'scripts/directives/addProgramType/set/exerciseType/addProgramTypeSetExerciseCardioView.html',
      restrict: 'E',
      scope: {
        setExercise: '='
      },
      link: function postLink() {
      }
    };
  });
