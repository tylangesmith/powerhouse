'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeSetExerciseWeighted
 * @description
 * # addProgramTypeSetExerciseWeighted
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeSetExerciseWeighted', function () {
    return {
      templateUrl: 'scripts/directives/addProgramType/set/exerciseType/addProgramTypeSetExerciseWeightedView.html',
      restrict: 'E',
      scope: {
        setExercise: '='
      },
      link: function postLink() {
      }
    };
  });
