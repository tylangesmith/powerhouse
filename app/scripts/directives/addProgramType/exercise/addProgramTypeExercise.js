'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeExercise
 * @description
 * # addProgramTypeExercise
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeExercise', ['addProgramTypeExerciseService', 'exerciseTypeService', function (addProgramTypeExerciseService, exerciseTypeService) {
    return {
      templateUrl: 'scripts/directives/addProgramType/exercise/addProgramTypeExerciseView.html',
      restrict: 'E',
      scope: {
        programTypeExercises: '='
      },
      link: function postLink(scope) {

        scope.helpAddProgramTypeExerciseTypeUrl = addProgramTypeExerciseService.helpAddProgramTypeExerciseTypeUrl;

        scope.exerciseTypes = exerciseTypeService.getExerciseTypes();

        // Options
        scope.remove = addProgramTypeExerciseService.remove;
        scope.edit = addProgramTypeExerciseService.edit;

        scope.$watchCollection(function(){
          return addProgramTypeExerciseService.exercises;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.programTypeExercises = addProgramTypeExerciseService.exercises;
          }
        });

        scope.addExercise = function(){
          scope.programTypeExercises = addProgramTypeExerciseService.addExercise(scope.programTypeExercises);
        };

        scope.confirmExercise = function(exercise){
          exercise.confirmed = true;
        };

        scope.removeExercise = function(exercise){
          scope.programTypeExercises = addProgramTypeExerciseService.removeExercise(scope.programTypeExercises, exercise);
        };

        scope.editExercise = function(exercise){
          exercise.confirmed = false;
        };

        scope.isInvalid = function(exercise){
          return addProgramTypeExerciseService.isInvalid(scope.programTypeExercises, exercise);
        };

        scope.hasConfirmed = function(){
          return addProgramTypeExerciseService.hasConfirmed(scope.programTypeExercises);
        };

      }
    };
  }]);
