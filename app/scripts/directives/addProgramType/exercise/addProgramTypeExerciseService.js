'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramTypeExerciseService
 * @description
 * # addProgramTypeExerciseService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramTypeExerciseService', ['utilService', 'toastService', 'addProgramTypeService', function (utilService, toastService, addProgramTypeService) {
    
    var contract = {
      helpAddProgramTypeExerciseTypeUrl: 'scripts/directives/addProgramType/help/helpAddProgramTypeExerciseTypeTemplate.html',
      exercises: [],
      remove: true,
      edit: true,
    };

    contract.addExercise = function(exercisesArray){
      exercisesArray.push(generateExercise(utilService.getUniqueId(exercisesArray)));
      return exercisesArray;
    };

    contract.removeExercise = function(exercisesArray, exercise){
      var tempExercisesArray = angular.copy(exercisesArray);
      toastService.showUndoToast('Exercise removed', function(){
        contract.exercises = tempExercisesArray;
        addProgramTypeService.exerciseRemoveUndone();
      });

      return utilService.removeFromArray(exercisesArray, exercise);
    };

    contract.isInvalid = function(exerciseArray, exercise){
      // Check that the name is not already taken
      // Check that an exercise type is selected
      return (exerciseTypeInvalid(exercise) || exerciseNameInvalid(exerciseArray, exercise));
    };

    contract.hasConfirmed = function(exerciseArray){
      for(var i = 0; i < exerciseArray.length; i++){
        if(exerciseArray[i].confirmed === true){
          return true;
        }
      }
      return false;
    };

    var exerciseTypeInvalid = function(exercise){
      return (utilService.isUndefined(exercise.exerciseType) || exercise.exerciseType === {} || 
        !exercise.exerciseType.hasOwnProperty('id') || !exercise.exerciseType.hasOwnProperty('name'));
    };

    var exerciseNameInvalid = function(exerciseArray, exercise){
      if(utilService.isUndefined(exercise.name) || exercise.name === ''){
        return true;
      }

      for(var i = 0; i < exerciseArray.length; i++){
        if(exercise.name === exerciseArray[i].name && exercise.id !== exerciseArray[i].id){
          return true;
        }
      }

      return false;
    };

    var generateExercise = function(id){
      return {
        id: id,
        name: '',
        exerciseType: {},
        description: '',
        confirmed: false
      };
    };

    return contract;
  }]);
