'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramTypeWeekService
 * @description
 * # addProgramTypeWeekService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramTypeWeekService', ['utilService', 'toastService', function (utilService, toastService) {
    var contract = {
      remove: true,
      edit: true,
      duplicate: true,
      move: true,
      weeks: []
    };

    contract.addWeek = function(weekArray){
      weekArray.push(generateWeek(utilService.getUniqueId(weekArray)));
      return weekArray;
    };

    contract.removeWeek = function(weeksArray, week){
      var tempWeeksArray = angular.copy(weeksArray);
      toastService.showUndoToast('Week removed', function(){
        contract.weeks = tempWeeksArray;
      });

      return utilService.removeFromArray(weeksArray, week);
    };

    contract.duplicateWeek = function(weekArray, week){
      var newWeek = angular.copy(week);
      newWeek.id = utilService.getUniqueId(weekArray);
      newWeek.name = newWeek.name + ' ' + newWeek.id;
      weekArray.push(newWeek);
      return weekArray;
    };

    contract.moveWeek = function(weekArray, week, direction){
      var moveFunctions = {
        up: function(rArray, week, index){
          if(index !== -1 && index !== 0){
            rArray[index] = rArray[index - 1];
            rArray[index - 1] = week;
          }
          return rArray;
        },
        down: function(rArray, week, index){
          if(index !== -1 && index !== (rArray.length - 1)){
            rArray[index] = rArray[index + 1];
            rArray[index + 1] = week;
          }
          return rArray;
        }
      };

      var rArray = weekArray;

      var index = weekArray.findIndex(function(element){
        return element.id === week.id;
      });
    
      return moveFunctions[direction](rArray, week, index);
    };

    contract.isInvalid = function(weekArray, week){
      return (isNameInvalid(weekArray, week) || isDaysInvalid(week));
    };

    contract.hasConfirmed = function(weeksArray){
      for(var i = 0; i < weeksArray.length; i++){
        if(weeksArray[i].confirmed === true){
          return true;
        }
      }
      return false;
    };

    var isNameInvalid = function(weekArray, week){
      return (utilService.isUndefined(week.name) || week.name === '' || isNameTaken(weekArray, week));
    };

    var isNameTaken = function(weekArray, week){
      for(var i = 0; i < weekArray.length; i++){
        if(weekArray[i].name === week.name && weekArray[i].id !== week.id){
          return true;
        }
      }
      return false;
    };

    var isDaysInvalid = function(week){
      var allNotConfirmed = function(days){
        for(var i = 0; i < days.length; i++){
          if(days[i].confirmed === false){
            return true;
          }
        }
        return false;
      };
      return (utilService.isUndefined(week.days) || week.days.length <= 0 || week.days.length > 7 || allNotConfirmed(week.days));
    };

    var generateWeek = function(id){
      return {
        id: id, 
        name: '',
        days: [],
        confirmed: false
      };
    };

    return contract;
  }]);
