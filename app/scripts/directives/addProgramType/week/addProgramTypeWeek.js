'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeWeek
 * @description
 * # addProgramTypeWeek
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeWeek', ['addProgramTypeWeekService', function (addProgramTypeWeekService) {
    return {
      templateUrl: 'scripts/directives/addProgramType/week/addProgramTypeWeekView.html',
      restrict: 'E',
      scope: {
        programTypeWeeks: '=',
      },
      link: function postLink(scope) {

        scope.remove = addProgramTypeWeekService.remove;
        scope.edit = addProgramTypeWeekService.edit;
        scope.duplicate = addProgramTypeWeekService.duplicate;
        scope.move = addProgramTypeWeekService.move;

        scope.$watchCollection(function(){
          return addProgramTypeWeekService.weeks;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.programTypeWeeks = addProgramTypeWeekService.weeks;
          }
        });

        scope.addWeek = function(){
          scope.programTypeWeeks = addProgramTypeWeekService.addWeek(scope.programTypeWeeks);
        };

        scope.editWeek = function(week){
          week.confirmed = false;
        };

        scope.confirmWeek = function(week){
          week.confirmed = true;
        };

        scope.removeWeek = function(week){
          scope.programTypeWeeks = addProgramTypeWeekService.removeWeek(scope.programTypeWeeks, week);
        };

        scope.duplicateWeek = function(week){
          scope.programTypeWeeks = addProgramTypeWeekService.duplicateWeek(scope.programTypeWeeks, week);
        };

        scope.moveWeek = function(week, direction){
          scope.programTypeWeeks = addProgramTypeWeekService.moveWeek(scope.programTypeWeeks, week, direction);
        };

        scope.hasConfirmed = function(){
          return addProgramTypeWeekService.hasConfirmed(scope.programTypeWeeks);
        };

        scope.isInvalid = function(week){
          return addProgramTypeWeekService.isInvalid(scope.programTypeWeeks, week);
        };
      }
    };
  }]);
