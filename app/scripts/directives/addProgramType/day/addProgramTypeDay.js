'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeDay
 * @description
 * # addProgramTypeDay
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeDay', ['addProgramTypeDayService', function (addProgramTypeDayService) {
    return {
      templateUrl: 'scripts/directives/addProgramType/day/addProgramTypeDayView.html',
      restrict: 'E',
      scope: {
        programTypeDays: '='
      },
      link: function postLink(scope) {
        
        scope.remove = addProgramTypeDayService.remove;
        scope.edit = addProgramTypeDayService.edit;
        scope.duplicate = addProgramTypeDayService.duplicate;
        scope.move = addProgramTypeDayService.move;

        scope.$watchCollection(function(){
          return addProgramTypeDayService.days;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.programTypeDays = addProgramTypeDayService.days;
          }
        });
        
        scope.addDay = function(){
          scope.programTypeDays = addProgramTypeDayService.addDay(scope.programTypeDays);
        };

        scope.editDay = function(day){
          day.confirmed = false;
        };

        scope.duplicateDay = function(day){
          scope.programTypeDays = addProgramTypeDayService.duplicateDay(scope.programTypeDays, day);
        };

        scope.moveDay = function(day, direction){
          scope.programTypeDays = addProgramTypeDayService.moveDay(scope.programTypeDays, day, direction);
        };

        scope.confirmDay = function(day){
          day.confirmed = true;
        };

        scope.removeDay = function(day){
          scope.programTypeDays = addProgramTypeDayService.removeDay(scope.programTypeDays, day);
        };

        scope.hasConfirmed = function(){
          return addProgramTypeDayService.hasConfirmed(scope.programTypeDays);
        };

        scope.isInvalid = function(day){
          return addProgramTypeDayService.isInvalid(scope.programTypeDays, day);
        };
      }
    };
  }]);
