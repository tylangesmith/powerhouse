'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramTypeDayService
 * @description
 * # addProgramTypeDayService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramTypeDayService', ['utilService', 'toastService', function (utilService, toastService) {
    var contract = {
      remove: true,
      edit: true,
      duplicate: true,
      move: true,
      days: []
    };

    contract.addDay = function(dayArray){
      dayArray.push(generateDay(utilService.getUniqueId(dayArray)));
      return dayArray;
    };

    contract.removeDay = function(daysArray, day){
      var tempDaysArray = angular.copy(daysArray);
      toastService.showUndoToast('Day removed', function(){
        contract.days = tempDaysArray;
      });

      return utilService.removeFromArray(daysArray, day);
    };

    contract.duplicateDay = function(dayArray, day){
      var newDay = angular.copy(day);
      newDay.id = utilService.getUniqueId(dayArray);
      newDay.name = newDay.name + ' ' + newDay.id;
      dayArray.push(newDay);
      return dayArray;
    };

    contract.moveDay = function(dayArray, day, direction){
      var moveFunctions = {
        up: function(rArray, day, index){
          if(index !== -1 && index !== 0){
            rArray[index] = rArray[index - 1];
            rArray[index - 1] = day;
          }
          return rArray;
        },
        down: function(rArray, day, index){
          if(index !== -1 && index !== (rArray.length - 1)){
            rArray[index] = rArray[index + 1];
            rArray[index + 1] = day;
          }
          return rArray;
        }
      };

      var rArray = dayArray;

      var index = dayArray.findIndex(function(element){
        return element.id === day.id;
      });
    
      return moveFunctions[direction](rArray, day, index);
    };

    contract.isInvalid = function(dayArray, day){
      return (isNameInvalid(dayArray, day) || isSetInvalid(day));
    };

    contract.hasConfirmed = function(dayArray){
      for(var i = 0; i < dayArray.length; i++){
        if(dayArray[i].confirmed === true){
          return true;
        }
      }
      return false;
    };

    var isNameInvalid = function(dayArray, day){
      // Check if name is not empty
      return (utilService.isUndefined(day.name) || day.name === '' || nameTaken(dayArray, day));
    };

    var nameTaken = function(dayArray, day){
      for(var i = 0; i < dayArray.length; i++){
        if(dayArray[i].name === day.name && dayArray[i].id !== day.id){
          return true;
        }
      }
      return false;
    };

    var isSetInvalid = function(day){
      var allNotConfirmed = function(sets){
        for(var i = 0; i < sets.length; i++){
          if(sets[i].confirmed === false){
            return true;
          }
        }
        return false;
      };
      return (utilService.isUndefined(day.sets) || day.sets.length <= 0 || allNotConfirmed(day.sets));
    };

    var generateDay = function(id){
      return {
        id: id,
        name: '',
        sets: [],
        confirmed: false
      };
    };

    return contract;
  }]);
