'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramTypeNameService
 * @description
 * # addProgramTypeNameService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramTypeHeaderService', ['programTypeLevelService', function (programTypeLevelService) {
    var contract = {
      helpAddProgramTypeUrl: 'scripts/directives/addProgramType/help/helpAddProgramTypeTemplate.html'
    };

    contract.getLevels = function(){
      return programTypeLevelService.getLevels();
    };

    return contract;
  }]);
