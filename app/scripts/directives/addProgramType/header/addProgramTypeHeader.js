'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramTypeName
 * @description
 * # addProgramTypeName
 */
angular.module('powerHouseApp')
  .directive('addProgramTypeHeader', ['addProgramTypeHeaderService', function (addProgramTypeNameService) {
    return {
      templateUrl: 'scripts/directives/addProgramType/header/addProgramTypeHeaderView.html',
      restrict: 'E',
      scope: {
        programTypeName: '=',
        level: '=',
        description: '='
      },
      link: function postLink(scope) {
        scope.helpAddProgramTypeUrl = addProgramTypeNameService.helpAddProgramTypeUrl;

        scope.levels = addProgramTypeNameService.getLevels();

        scope.$watch(function(){
          return scope.programTypeName;
        }, function(newValue, oldValue){
          if(newValue !== oldValue && scope.programTypeName === ''){
            scope.programTypeDetailsForm.$setUntouched();
          }
        });
      }
    };
  }]);
