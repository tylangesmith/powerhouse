'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramType
 * @description
 * # addProgramType
 */
angular.module('powerHouseApp')
  .directive('addProgramType', ['addProgramTypeService', function (addProgramTypeService) {
    return {
      templateUrl: 'scripts/directives/addProgramType/addProgramTypeView.html',
      restrict: 'E',
      scope: {
        programTypeName: '=',
        description: '=',
        level: '=',
        exercises: '=',
        weeks: '=',
        addFunction: '=',
        removeFunction: '=',
        invalidFunction: '='
      },
      link: function postLink(scope) {

        addProgramTypeService.weeks = scope.weeks;
        addProgramTypeService.exercises = scope.exercises;

        scope.$watchCollection(function(){
          return scope.exercises;
        }, function(newValue, oldValue){
          addProgramTypeService.exercises = scope.exercises;
          // Exercise removed
          if(newValue.length < oldValue.length){
            addProgramTypeService.weeks = addProgramTypeService.exerciseRemoved(scope.exercises, scope.weeks);
          }
        });

        scope.$watchCollection(function(){
          return addProgramTypeService.weeks;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.weeks = newValue;
          }
        });
      }
    };
  }]);
