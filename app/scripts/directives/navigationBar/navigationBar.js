'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:NavigationBar
 * @description
 * # NavigationBar
 */
angular.module('powerHouseApp')
  .directive('navigationBar', ['navigationBarService', function (navigationBarService) {
    return {
      templateUrl: 'scripts/directives/navigationBar/navigationBarView.html',
      restrict: 'E',
      link: function postLink(scope) {

        scope.sideMenuItems = navigationBarService.sideMenuItems;        

        scope.toggleSidenav = function(){
          navigationBarService.toggleSidenav();
        };
      }
    };
  }]);
