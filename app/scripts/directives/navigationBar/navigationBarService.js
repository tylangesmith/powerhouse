'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.navigationBarService
 * @description
 * # navigationBarService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('navigationBarService', ['sideNavigationService', function (sideNavigationService) {
    var contract = {
      id: 'sidenav',
      sideMenuItems: [
        {
          text: 'Settings',
          icon: 'images/icons/settingsBlack.svg',
          href: '#/settings'
        },
        {
          text: 'ORM Calculator',
          icon: 'images/icons/percent.svg',
          href: '#/one-rep-max-calculator'
        },
        {
          text: 'Barbell Plate Calculator',
          icon: 'images/icons/barbellBlack.svg',
          href: '#/barbell-plate-calculator'
        },        
        {
          text: 'Help',
          icon: 'images/icons/info.svg',
          href: '#/help'
        },
        {
          text: 'Contact',
          icon: 'images/icons/email.svg',
          href: '#/contact'
        },
        {
          text: 'Upgrade',
          icon: 'images/icons/moneyBlack.svg',
          href: '#/upgrade'
        }
      ]
    };

    contract.toggleSidenav = function(){
      sideNavigationService.toggleSidenav(contract.id);
    };

    return contract;
  }]);
