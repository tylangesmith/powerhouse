'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:dashboardProgramList
 * @description
 * # dashboardProgramList
 */
angular.module('powerHouseApp')
  .directive('dashboardProgramList', [function () {
    return {
      templateUrl: 'scripts/directives/dashboardProgramList/dashboardProgramListView.html',
      restrict: 'E',
      scope: {
        programs: '='
      },
      link: function postLink(scope) {
        scope.emptyListMessage = 'No currently active programs';
        scope.emptyListButtonText = 'Add Program';
        scope.emptyListButtonList = '#/add-program';
        scope.helpActiveProgramListUrl = 'scripts/directives/dashboardProgramList/helpDashboardProgramListTemplate.html';
      }
    };
  }]);
