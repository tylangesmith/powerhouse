'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:highlightCard
 * @description
 * # highlightCard
 */
angular.module('powerHouseApp')
  .directive('highlightCard', function () {
    return {
      templateUrl: 'scripts/directives/highlightCard/highlightCardView.html',
      restrict: 'E',
      scope: {
        headerText: '=',
        highlightText: '=',
        highlightColor: '=',
        subheadText: '='
      },
      link: function postLink() {
        
      }
    };
  });
