'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:bottomNavigationBar
 * @description
 * # bottomNavigationBar
 */
angular.module('powerHouseApp')
  .directive('bottomNavigationBar', function () {
    return {
      templateUrl: 'scripts/directives/bottomNavigationBar/bottomNavigationBarView.html',
      restrict: 'E',
      link: function postLink() {
        
      }
    };
  });
