'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.listFilterService
 * @description
 * # listFilterService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('listFilterService', ['$filter', function ($filter) {
    var contract = {};

    contract.filter = function(originalValues, filterValue, property){
      return $filter('filter')(originalValues, combinedPropertyAndFilterValue(filterValue, property));
    };

    var combinedPropertyAndFilterValue = function(filterValue, property){
      var rObject = {};
      rObject[property] = filterValue;
      return rObject;
    };

    return contract;
  }]);
