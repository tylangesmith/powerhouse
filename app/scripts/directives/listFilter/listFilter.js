'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:listFilter
 * @description
 * # listFilter
 */
angular.module('powerHouseApp')
  .directive('listFilter', ['listFilterService', function (listFilterService) {
    return {
      templateUrl: 'scripts/directives/listFilter/listFilterView.html',
      restrict: 'E',
      scope: {
        values: '=',
        originalValues: '=',
        property: '='
      },
      link: function postLink(scope) {

        scope.filterValue = '';

        scope.$watch(function(){
          return scope.filterValue;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.values = listFilterService.filter(scope.originalValues, scope.filterValue, scope.property);
          }
        });

        scope.clear = function(){
          scope.filterValue = '';
        };
      }
    };
  }]);
