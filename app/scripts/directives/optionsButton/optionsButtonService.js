'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.optionsButtonService
 * @description
 * # optionsButtonService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('optionsButtonService', function () {
    var contract = {
      direction: 'left'
    };

    return contract;
  });
