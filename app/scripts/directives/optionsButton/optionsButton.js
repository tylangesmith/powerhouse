'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:optionsButton
 * @description
 * # optionsButton
 */
angular.module('powerHouseApp')
  .directive('optionsButton', ['optionsButtonService', function (optionsButtonService) {
    return {
      templateUrl: 'scripts/directives/optionsButton/optionsButtonView.html',
      restrict: 'E',
      scope: {
        value: '=',
        remove: '=',
        removeFunction: '=',
        edit: '=',
        editFunction: '=',
        duplicate: '=',
        duplicateFunction: '=',
        move: '=',
        moveFunction: '='
      },
      link: function postLink(scope) {
        scope.direction = optionsButtonService.direction;
        scope.moving = false;
        scope.isOpen = false;

        scope.moveClicked = function(){
          scope.moving = true;
        };

        scope.moveDone = function(){
          scope.moving = false;
        };

        scope.moveUp = function(){
          scope.moveFunction(scope.value, 'up');
        };

        scope.moveDown = function(){
          scope.moveFunction(scope.value, 'down');
        };

      }
    };
  }]);
