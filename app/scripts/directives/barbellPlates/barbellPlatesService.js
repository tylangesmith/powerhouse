'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.barbellPlatesService
 * @description
 * # barbellPlatesService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('barbellPlatesService', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
