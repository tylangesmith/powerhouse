'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:barbellPlates
 * @description
 * # barbellPlates
 */
angular.module('powerHouseApp')
  .directive('barbellPlates', function () {
    return {
      templateUrl: 'scripts/directives/barbellPlates/barbellPlatesView.html',
      restrict: 'E',
      scope: {
        unit: '=',
        plates: '='
      },
      link: function postLink() {
      }
    };
  });
