'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:listEmpty
 * @description
 * # listEmpty
 */
angular.module('powerHouseApp')
  .directive('listEmpty', function () {
    return {
      templateUrl: 'scripts/directives/listEmpty/listEmptyView.html',
      restrict: 'E',
      scope: {
        message: '=',
        buttonText: '=',
        buttonLink: '='
      },
      link: function postLink() {
        
      }
    };
  });
