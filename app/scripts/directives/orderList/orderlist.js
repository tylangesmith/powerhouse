'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:orderList
 * @description
 * # orderList
 */
angular.module('powerHouseApp')
  .directive('orderList', ['utilService', 'orderListService', function (utilService, orderListService) {
    return {
      templateUrl: 'scripts/directives/orderList/orderListView.html',
      restrict: 'E',
      scope: {
        key: '=',
        reverseKey: '=',
        orderValues: '=',
        originalValues: '=',
        values: '='
      },
      link: function postLink(scope) {
        // Get default / persisted values
        scope.reversed = orderListService.getReversedValue(scope.reverseKey);
        scope.selectedOrder = orderListService.getOrderValue(scope.key);
        scope.lastSelected = scope.selectedOrder;

        // If orderChanges
        scope.$watch(function(){
          return scope.selectedOrder;
        }, function(newValue, oldValue){
          // Check that we have a change
          if(newValue !== oldValue){
            // Check that the selectedOrder is different to the stored one 
            if(scope.selectedOrder.text !== orderListService.getOrderValue(scope.key).text){
              orderListService.orderValueChanged(scope.key, scope.selectedOrder);
              scope.reversed = false;
              scope.values = orderListService.orderBy(scope.selectedOrder, scope.originalValues, scope.reversed);
              scope.lastSelected = scope.selectedOrder;
            }
            else if(scope.selectedOrder.text === orderListService.getOrderValue(scope.key).text){
              // sort the values
              scope.values = orderListService.orderBy(scope.selectedOrder, scope.originalValues, scope.reversed);
            }
          }
        });

        scope.$watch(function(){
          return scope.reversed;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            if(scope.reversed !== orderListService.getReversedValue(scope.reverseKey)){
              orderListService.orderValueReverseChanged(scope.reverseKey, scope.reversed);
            }
          }
        });

        scope.reverse = function(){
          scope.reversed = !scope.reversed;
          scope.values = orderListService.orderBy(scope.selectedOrder, scope.originalValues, scope.reversed);
        };

        scope.$watch(function(){
          return scope.originalValues;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.values = orderListService.orderBy(scope.selectedOrder, scope.originalValues, scope.reversed);
          }
        });
      }
    };
  }]);
