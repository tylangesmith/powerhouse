'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.orderListService
 * @description
 * # orderListService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('orderListService', ['$filter', 'utilService', 'storageService', function ($filter, utilService, storageService) {
    var contract = {
      defaultValue: {
        prop: 'id',
        text: 'None'
      }
    };

    contract.getOrderByValues = function(values){
      return [contract.defaultValue].concat(values);
    };

    contract.getOrderValue = function(key){
      return getOrderValueFromStorage(key);
    };

    contract.getReversedValue = function(key){
      return getReversedFromStorage(key);
    };

    contract.orderBy = function(selectedOrder, values, reverse){
      if(utilService.isDefined(selectedOrder.comparator)){
        return $filter('orderBy')(values, selectedOrder.prop, reverse, selectedOrder.comparator);  
      }
      return $filter('orderBy')(values, selectedOrder.prop, reverse);
    };

    contract.orderValueChanged = function(key, value){
      storeOrderValue(key, value);
    };

    contract.orderValueReverseChanged = function(key, value){
      storeReversedValue(key, value);
    };

    var getOrderValueFromStorage = function(key){
      return storageService.getValueOrDefault(key, contract.defaultValue);
    };


    var getReversedFromStorage = function(key){
      return storageService.getValueOrDefault(key, false);
    };

    var storeOrderValue = function(key, value){
      storageService.storeValue(key, value);
    };

    var storeReversedValue = function(key, value){
      storageService.storeValue(key, value);
    };

    return contract;
  }]);
