'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramHeaderService
 * @description
 * # addProgramHeaderService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramHeaderService', ['addProgramService', 'programTypeService', function (addProgramService, programTypeService) {
    var contract = {
      helpAddProgramUrl: 'scripts/directives/addProgram/helpAddProgramTemplate.html',
      helpAddProgramProgramIncrementUrl: 'scripts/directives/addProgram/helpAddProgramProgramIncrementTemplate.html'
    };

    contract.getProgramTypes = function(programType){
      var programTypes = angular.copy(programTypeService.getProgramTypes());
      if(programTypeService.validProgramType(programType) && programTypeService.containsProgramType(programTypes, programType) === false){
        programTypes.push(programType);
      }
      return programTypes;
    };

    return contract;
  }]);
