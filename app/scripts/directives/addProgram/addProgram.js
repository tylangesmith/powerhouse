'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgram
 * @description
 * # addProgram
 */
angular.module('powerHouseApp')
  .directive('addProgram', [function () {
    return {
      templateUrl: 'scripts/directives/addProgram/addProgramView.html',
      restrict: 'E',
      scope: {
        programName: '=',
        programType: '=',
        increment: '=',
        exercises: '=',
        addFunction: '=',
        removeFunction: '=',
        invalidFunction: '='
      },
      link: function postLink() {

      }
    };
  }]);
