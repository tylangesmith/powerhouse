'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramExercise
 * @description
 * # addProgramExercise
 */
angular.module('powerHouseApp')
  .directive('addProgramExercise', ['addProgramExerciseService', function (addProgramExerciseService) {
    return {
      templateUrl: 'scripts/directives/addProgram/addProgramExerciseView.html',
      restrict: 'E',
      scope: {
        exercises: '='
      },
      link: function postLink(scope) {
        scope.helpAddProgramOneRepMaxUrl = addProgramExerciseService.helpAddProgramOneRepMaxUrl;
      }
    };
  }]);
