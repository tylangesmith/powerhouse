'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.addProgramExerciseService
 * @description
 * # addProgramExerciseService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('addProgramExerciseService', function () {
    var contract = {
      helpAddProgramOneRepMaxUrl: 'scripts/directives/addProgram/helpAddProgramOneRepMaxTemplate.html'
    };

    return contract;
  });
