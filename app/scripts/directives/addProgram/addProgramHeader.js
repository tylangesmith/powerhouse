'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addProgramHeader
 * @description
 * # addProgramHeader
 */
angular.module('powerHouseApp')
  .directive('addProgramHeader', ['addProgramHeaderService', 'unitService', function (addProgramHeaderService, unitService) {
    return {
      templateUrl: 'scripts/directives/addProgram/addProgramHeaderView.html',
      restrict: 'E',
      scope: {
        programName: '=',
        programType: '=',
        increment: '='
      },
      link: function postLink(scope) {
        scope.programTypes = addProgramHeaderService.getProgramTypes(scope.programType);
        scope.unit = unitService.getCurrentUnit();

        scope.helpAddProgramUrl = addProgramHeaderService.helpAddProgramUrl;
        scope.helpAddProgramProgramIncrementUrl = addProgramHeaderService.helpAddProgramProgramIncrementUrl;
      }
    };
  }]);
