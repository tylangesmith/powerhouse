'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:messageCard
 * @description
 * # messageCard
 */
angular.module('powerHouseApp')
  .directive('messageCard', ['$location', function ($location) {
    return {
      templateUrl: 'scripts/directives/messageCard/messageCardView.html',
      restrict: 'E',
      scope: {
        message: '=',
        buttonText: '=',
        link: '='
      },
      link: function postLink(scope) {
        scope.buttonClicked = function(){
          $location.path(scope.link);
        };
      }
    };
  }]);
