'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:viewProgramType
 * @description
 * # viewProgramType
 */
angular.module('powerHouseApp')
  .directive('programTypeList', ['$location', 'programTypeListService', 'programTypeService', function ($location, programTypeListService, programTypeService) {
    return {
      templateUrl: 'scripts/directives/programTypeList/ProgramTypeListView.html',
      restrict: 'E',
      scope: {
        programTypes: '='
      },
      link: function postLink(scope) {
        scope.formattedProgramTypes = programTypeListService.formatProgramTypes(scope.programTypes);

        scope.editFunction = function(programType){
          $location.path('edit-program-type/' + programType.id);
        };

        scope.removeFunction = function(programType){
          programTypeService.removeProgramType(programType.programType);
        };

        scope.$watch(function(){
          return scope.programTypes;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            scope.formattedProgramTypes = programTypeListService.formatProgramTypes(scope.programTypes);
          }
        }, true);

      }
    };
  }]);
