'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.programTypeListService
 * @description
 * # programTypeListService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('programTypeListService', ['keyHandlerService', 'orderListService', function (keyHandlerService, orderListService) {
    var contract = {
      orderKey: keyHandlerService.keys.programTypeListOrderKey,
      reverseKey: keyHandlerService.keys.programTypeListReversedKey,
      orderValues: [
        {
          prop: 'programTypeName',
          text: 'Name'
        },
        {
          prop: 'level',
          text: 'Experience Level',
          comparator: function(v1, v2){
            var rValue = 0;
            if(v1.value.id < v2.value.id){
              rValue = -1;
            }
            else if(v1.value.id > v2.value.id){
              rValue = 1;
            }
            return rValue;
          }
        },
        {
          prop: 'default',
          text: 'Default'
        },
        {
          prop: 'totalNumberOfSets',
          text: 'Number of Sets',

        },
        {
          prop: 'weeks',
          text: 'Number of Weeks',
          comparator: function(v1, v2){
            var rValue = 0;
            if(v1.value.length < v2.value.length){
              rValue = -1;
            }
            else if(v1.value.length > v2.value.length){
              rValue = 1;
            }
            return rValue;
          }
        },
      ]
    };

    contract.getOrderValues = function(){
      return orderListService.getOrderByValues(contract.orderValues);
    };

    contract.formatProgramTypes = function(programTypes){
      return programTypes.map(function(programType){
        return formatProgramType(programType);
      });
    };

    var formatProgramType = function(programType){
        return {
          programType: programType,
          id: programType.id,
          text: programType.programTypeName,
          secondText: 'Total Weeks: ' + programType.weeks.length + ', Total Sets: ' + programType.totalNumberOfSets,
          thirdText: 'Experience Level: ' + programType.level.name,
          href: '#/program-type-information/' + programType.id,
          removable: !programType.default,
          editable: true,
          color: 'grey-A100'
        };
    };

    return contract;
  }]);
