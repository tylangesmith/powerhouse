'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:weightUnitSetting
 * @description
 * # weightUnitSetting
 */
angular.module('powerHouseApp')
  .directive('weightUnitSetting', ['weightUnitSettingService', 'unitService', function (weightUnitSettingService, unitService) {
    return {
      templateUrl: 'scripts/directives/weightUnitSetting/weightUnitSettingView.html',
      restrict: 'E',
      link: function postLink(scope) {
        scope.units = weightUnitSettingService.getUnits();
        scope.currentUnit = weightUnitSettingService.getCurrentUnit();

        scope.$watch(function(){
          return scope.currentUnit;
        }, function(newValue, oldValue){
          if(newValue !== oldValue){
            weightUnitSettingService.changeUnit(newValue);
          }
        }, true);

        scope.$watch(function(){
          return unitService.currentUnit;
        }, function(newValue, oldValue){
          if(newValue !== oldValue && scope.currentUnit.name !== newValue){
            scope.currentUnit = weightUnitSettingService.getCurrentUnit();
          }
        });

      }
    };
  }]);
