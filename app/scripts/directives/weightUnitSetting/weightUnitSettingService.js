'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.weightUnitSettingService
 * @description
 * # weightUnitSettingService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('weightUnitSettingService', ['unitService', 'programService', function (unitService, programService) {
    var contract = {};

    contract.changeUnit = function(unit){
      unitService.changeUnit(unit.name);
      programService.convertPrograms();
    };

    contract.getUnits = function(){
      return angular.copy(unitService.getUnits());
    };

    contract.getCurrentUnit = function(){
      return angular.copy(unitService.getCurrentUnit());
    };

    return contract;
  }]);
