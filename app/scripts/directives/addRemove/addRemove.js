'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:addRemove
 * @description
 * # addRemove
 */
angular.module('powerHouseApp')
  .directive('addRemove', function () {
    return {
      templateUrl: 'scripts/directives/addRemove/addRemoveView.html',
      restrict: 'E',
      scope: {
        addFunction: '&',
        removeFunction: '&',
        invalidFunction: '&',
        name: '='
      },
      link: function postLink() {
      }
    };
  });
