'use strict';

/**
 * @ngdoc service
 * @name powerHouseApp.resetSettingService
 * @description
 * # resetSettingService
 * Service in the powerHouseApp.
 */
angular.module('powerHouseApp')
  .service('resetSettingService', ['$mdDialog', 'programService', 'programTypeService', 'recentlyActiveService', 'unitService', 'storageService', function ($mdDialog, programService, programTypeService, recentlyActiveService, unitService, storageService) {
    var contract = {
      helpResetSetting: 'scripts/directives/resetSetting/helpResetSetting.html'
    };

    contract.resetClicked = function(){
      var confirm = $mdDialog.confirm()
        .title('Reset')
        .textContent('Are you sure you want to reset, this action cannot be undone.')
        .ariaLabel('Reset')
        .ok('Reset')
        .cancel('Cancel');

        $mdDialog.show(confirm)
          .then(function(){
          reset();
        });
    };

    var reset = function(){
      programService.reset();
      programTypeService.reset();
      recentlyActiveService.reset();
      unitService.reset();
      storageService.reset();
    };

    return contract;
  }]);
