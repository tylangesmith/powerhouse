'use strict';

/**
 * @ngdoc directive
 * @name powerHouseApp.directive:resetSetting
 * @description
 * # resetSetting
 */
angular.module('powerHouseApp')
  .directive('resetSetting', ['resetSettingService', function (resetSettingService) {
    return {
      templateUrl: 'scripts/directives/resetSetting/resetSettingView.html',
      restrict: 'E',
      link: function postLink(scope) {
        scope.helpResetSetting = resetSettingService.helpResetSetting;

        scope.resetClicked = function(){
          resetSettingService.resetClicked();
        };
      }
    };
  }]);
